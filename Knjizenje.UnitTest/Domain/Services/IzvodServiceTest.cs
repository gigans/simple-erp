﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.UnitTest.Domain.Services
{
    public class IzvodServiceTest
	{
		[Fact]
		public void FormirajStavkeNaloga_StavkeIzvodaNull_Greska()
		{
			var fakeKontoRepo = new Mock<IKontoRepository>();
			var izvodSvc = new IzvodService(fakeKontoRepo.Object);

			Func<Task> formiraj = async () => { await izvodSvc.FormirajStavkeNaloga(null); };

			Assert.ThrowsAsync<ArgumentNullException>(formiraj);
		}

		[Fact]
		public async Task FormirajStavkeNaloga_StavkeIzvodaPrazno_VracaPrazno()
		{
			var fakeKontoRepo = new Mock<IKontoRepository>();
			var izvodSvc = new IzvodService(fakeKontoRepo.Object);
			var stavkeIzvoda = Enumerable.Empty<StavkaIzvoda>();

			var stavkeNaloga = await izvodSvc.FormirajStavkeNaloga(stavkeIzvoda);

			Assert.Empty(stavkeNaloga);
		}

		[Fact]
		public async Task FormirajStavkeNaloga_Korektno()
		{
			var fakeKontoRepo = new Mock<IKontoRepository>();
			fakeKontoRepo.Setup(x => x.GetBySifraAsync("435"))
				.Returns(()=> {
					var konto = new Mock<Konto>("435", null);
					konto.SetupGet(x => x.Id).Returns(435);
					return Task.FromResult(konto.Object);
				});
			fakeKontoRepo.Setup(x => x.GetBySifraAsync("241"))
				.Returns(() => {
					var konto = new Mock<Konto>("241", null);
					konto.SetupGet(x => x.Id).Returns(241);
					return Task.FromResult(konto.Object);
				});
			var izvodSvc = new IzvodService(fakeKontoRepo.Object);
			var stavkeIzvoda = new[]
			{
				new StavkaIzvoda(221, 200, 0),
				new StavkaIzvoda(221, 0, 143)
			};

			var stavkeNaloga = await izvodSvc.FormirajStavkeNaloga(stavkeIzvoda);

			Assert.Equal(4, stavkeNaloga.Count);
			Assert.Contains(stavkeNaloga, x => x.IdKonto == 435 && x.Iznos.Duguje == 200);
			Assert.Contains(stavkeNaloga, x => x.IdKonto == 241 && x.Iznos.Potrazuje == 200);
			Assert.Contains(stavkeNaloga, x => x.IdKonto == 435 && x.Iznos.Potrazuje == 143);
			Assert.Contains(stavkeNaloga, x => x.IdKonto == 241 && x.Iznos.Duguje == 143);
		}
	}
}
