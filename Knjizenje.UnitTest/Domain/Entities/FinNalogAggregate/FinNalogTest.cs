﻿using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.Exceptions;
using Knjizenje.Domain.SeedWork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Xunit;

namespace Knjizenje.UnitTest.Domain.Entities.FinNalogAggregate
{
	public class FinNalogTest
	{
		DateTime PodrazumevaniDatumNaloga = new DateTime(2018, 10, 1);
		TipNaloga PodrazumevaniTipNaloga = TipNaloga.UlazneFakture;
		string PodrazumevaniOpisNaloga = "test nalog";


		[Fact]
		public void NoviNalog_TipNull_Greska()
		{
			Action noviNalog = () =>
			{
				FinNalog.OtvoriNovi(null, new DateTime(2018, 9, 1),
					null,
					new[]
					{
						FinStavka.Nova(34, 500, 0, null)
					});
			};

			Assert.Throws<ArgumentNullException>(noviNalog);
		}

		[Fact]
		public void NoviNalog_StavkeNull_Greska()
		{
			Action noviNalog = () =>
			{
				FinNalog.OtvoriNovi(TipNaloga.UlazneFakture, new DateTime(2018, 9, 1),
					null,
					null);
			};

			Assert.Throws<ArgumentNullException>(noviNalog);
		}

		[Fact]
		public void NoviNalog_StavkePrazno_Greska()
		{
			Action noviNalog = () =>
			{
				FinNalog.OtvoriNovi(TipNaloga.UlazneFakture, new DateTime(2018, 9, 1),
					null,
					Enumerable.Empty<FinStavka>());
			};

			Assert.Throws<KnjizenjeException>(noviNalog);
		}

		[Fact]
		public void NoviNalog_JednaStavka_DodajeEvente()
		{
			var noviNalog = GetPodrazumevaniNalog(new[]
			{
				FinStavka.Nova(1, 100, 0, "opis test stavke")
			});

			Assert.Equal(2, noviNalog.UncommittedEvents.Count);
			Assert.Collection(noviNalog.UncommittedEvents.OfType<NalogOtvoren>(),
				e =>
				{
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(noviNalog.Id, e.IdNaloga);
					Assert.Equal(PodrazumevaniDatumNaloga, e.DatumNaloga);
					Assert.Equal(PodrazumevaniTipNaloga.Id, e.IdTip);
					Assert.Equal(PodrazumevaniOpisNaloga, e.Opis);
				});

			Assert.Collection(noviNalog.UncommittedEvents.OfType<StavkaProknjizena>(),
				e =>
				{
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(noviNalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdStavke);
					Assert.Equal(DateTime.Today, e.DatumKnjizenja);
					Assert.Equal(1, e.IdKonto);
					Assert.Equal(100, e.Duguje);
					Assert.Equal(0, e.Potrazuje);
					Assert.Equal("opis test stavke", e.Opis);
				});
		}

		[Fact]
		public void NoviNalog_ViseStavki_DodajeEvente()
		{
			var noviNalog = GetPodrazumevaniNalog(new[]
			{
				FinStavka.Nova(1, 100, 0, "opis test stavke1"),
				FinStavka.Nova(2, 0, 200, "opis test stavke2")
			});

			Assert.Equal(3, noviNalog.UncommittedEvents.Count);
			Assert.Collection(noviNalog.UncommittedEvents.OfType<NalogOtvoren>(),
				e =>
				{
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(noviNalog.Id, e.IdNaloga);
					Assert.Equal(PodrazumevaniDatumNaloga, e.DatumNaloga);
					Assert.Equal(PodrazumevaniTipNaloga.Id, e.IdTip);
					Assert.Equal(PodrazumevaniOpisNaloga, e.Opis);
				});
			Assert.Collection(noviNalog.UncommittedEvents.OfType<StavkaProknjizena>(),
				e =>
				{
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(noviNalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdStavke);
					Assert.Equal(DateTime.Today, e.DatumKnjizenja);
					Assert.Equal(1, e.IdKonto);
					Assert.Equal(100, e.Duguje);
					Assert.Equal(0, e.Potrazuje);
					Assert.Equal("opis test stavke1", e.Opis);
				},
				e =>
				{
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(noviNalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdStavke);
					Assert.Equal(DateTime.Today, e.DatumKnjizenja);
					Assert.Equal(2, e.IdKonto);
					Assert.Equal(0, e.Duguje);
					Assert.Equal(200, e.Potrazuje);
					Assert.Equal("opis test stavke2", e.Opis);
				});
		}

		[Fact]
		public void Zakljucaj_DodajeEvent()
		{
			var nalog = GetNalog();

			nalog.Zakljucaj();

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<NalogZakljucan>(),
				e =>
				{
					Assert.Equal(nalog.Id, e.IdNaloga);
				});
		}

		[Fact]
		public void Otkljucaj_DodajeEvent()
		{
			var nalog = GetNalog();
			nalog.Zakljucaj();
			nalog.ClearUncommittedEvents();

			nalog.Otkljucaj();

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<NalogOtkljucan>(),
				e =>
				{
					Assert.Equal(nalog.Id, e.IdNaloga);
				});
		}

		[Fact]
		public void Obrisi_DodajeEvent()
		{
			var nalog = GetNalog();

			nalog.Obrisi();

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<NalogObrisan>(),
				e =>
				{
					Assert.Equal(nalog.Id, e.IdNaloga);
				});
		}

		[Fact]
		public void Primeni_NalogOtvoren()
		{
			string idNaloga = $"FinNalog-{Guid.NewGuid()}";
			FinNalog nalog = new FinNalog();

			nalog.Primeni(new NalogOtvoren(new FinNalogId(idNaloga), new DateTime(2018, 10, 12), 1, "opis naloga"));

			Assert.Equal(idNaloga, nalog.Id.IdAsString());
		}

		[Fact]
		public void Primeni_NalogZakljucan()
		{
			var nalog = GetNalog();

			nalog.Primeni(new NalogZakljucan(nalog.Id));

			Assert.True(nalog.Zakljucan);
		}

		[Fact]
		public void Primeni_NalogOtkljucan()
		{
			var nalog = GetNalog();
			nalog.Primeni(new NalogZakljucan(nalog.Id));

			nalog.Primeni(new NalogOtkljucan(nalog.Id));

			Assert.False(nalog.Zakljucan);
		}

		[Fact]
		public void Primeni_NalogObrisan()
		{
			var nalog = GetNalog();

			nalog.Primeni(new NalogObrisan(nalog.Id));

			Assert.True(nalog.Obrisan);
		}

		[Fact]
		public void Primeni_StavkaProknjizena()
		{
			var nalog = GetNalog();
			var @event = new StavkaProknjizena(nalog.Id, Guid.NewGuid(),
				new DateTime(2018, 5, 3), 4, 789, 0, "opis");
			int ocekivaniBrojStavki = nalog.Stavke.Count + 1;
			var idPostojeceStavke = nalog.Stavke.Single().Id;

			nalog.Primeni(@event);

			Assert.Equal(ocekivaniBrojStavki, nalog.Stavke.Count);
			Assert.Collection(nalog.Stavke,
				s => Assert.Equal(idPostojeceStavke, s.Id),
				s => Assert.Equal(@event.IdStavke, s.Id));
		}

		[Fact]
		public void Primeni_StavkaUklonjena()
		{
			var nalog = GetNalog_SaViseStavki();
			var stavka = nalog.Stavke.First();
			var @event = new StavkaUklonjena(nalog.Id, stavka.Id, stavka.DatumKnjizenja, 
				stavka.IdKonto, stavka.Iznos.Duguje, stavka.Iznos.Potrazuje, stavka.Opis);
			int ocekivaniBrojStavki = nalog.Stavke.Count - 1;

			nalog.Primeni(@event);

			Assert.Equal(ocekivaniBrojStavki, nalog.Stavke.Count);
			Assert.DoesNotContain(nalog.Stavke, s => s.Id == @event.IdStavke);
		}

		[Fact]
		public void ProknjiziStavku_StavkaNull_Greska()
		{
			var nalog = GetNalog();

			Action proknjizi = () => { nalog.ProknjiziStavku(null); };

			Assert.Throws<ArgumentNullException>(proknjizi);
		}

		[Fact]
		public void ProknjiziStavku_NalogZakljucan_Greska()
		{
			var nalog = GetNalog();
			nalog.Primeni(new NalogZakljucan(nalog.Id));

			Action proknjizi = () => { nalog.ProknjiziStavku(FinStavka.Nova(3, 400, 0, "opis")); };

			Assert.Throws<KnjizenjeException>(proknjizi);
		}

		[Theory]
		[InlineData(587.5, 0)]
		[InlineData(-36.7, 0)]
		[InlineData(0, 986.2)]
		[InlineData(0, -520089)]
		public void ProknjiziStavku_DodajeEvent(decimal dug, decimal pot)
		{
			var nalog = GetNalog();

			nalog.ProknjiziStavku(FinStavka.Nova(56, dug, pot, "opis proknjizene stavke"));

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<StavkaProknjizena>(),
				e =>
				{
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(nalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdStavke);
					Assert.Equal(DateTime.Today, e.DatumKnjizenja);
					Assert.Equal(56, e.IdKonto);
					Assert.Equal(dug, e.Duguje);
					Assert.Equal(pot, e.Potrazuje);
					Assert.Equal("opis proknjizene stavke", e.Opis);
				});
		}

		[Fact]
		public void UkloniStavku_StavkaNull_Greska()
		{
			var nalog = GetNalog_SaViseStavki();

			Action ukloni = () => { nalog.UkloniStavku(null); };

			Assert.Throws<ArgumentNullException>(ukloni);
		}

		[Fact]
		public void UkloniStavku_NalogZakljucan_Greska()
		{
			var nalog = GetNalog_SaViseStavki();
			var stavka = nalog.Stavke.First();
			nalog.Primeni(new NalogZakljucan(nalog.Id));

			Action ukloni = () =>
			{
				nalog.UkloniStavku(stavka);
			};

			Assert.Throws<KnjizenjeException>(ukloni);
		}

		[Fact]
		public void UkloniStavku_NalogImaJednuStavku_Greska()
		{
			var nalog = GetNalog();
			var stavka = nalog.Stavke.First();

			Action ukloni = () =>
			{
				nalog.UkloniStavku(stavka);
			};

			Assert.Throws<KnjizenjeException>(ukloni);
		}

		[Fact]
		public void UkloniStavku_StavkaNePripadaNalogu_Greska()
		{
			var nalog = GetNalog_SaViseStavki();
			var stavka = FinStavka.Nova(65, 9258, 0, null);

			Action ukloni = () =>
			{
				nalog.UkloniStavku(stavka);
			};

			Assert.Throws<KnjizenjeException>(ukloni);
		}

		[Fact]
		public void UkloniStavku_DodajeEvent()
		{
			var nalog = GetNalog_SaViseStavki();
			nalog.ClearUncommittedEvents();
			var stavka = nalog.Stavke.First();

			nalog.UkloniStavku(stavka);

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<StavkaUklonjena>(),
				e =>
				{
					Assert.Equal(nalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(stavka.Id, e.IdStavke);
					Assert.NotEqual(Guid.Empty, e.IdStavke);
					Assert.Equal(stavka.DatumKnjizenja, e.DatumKnjizenja);
					Assert.Equal(stavka.IdKonto, e.IdKonto);
					Assert.Equal(stavka.Iznos.Duguje, e.Duguje);
					Assert.Equal(stavka.Iznos.Potrazuje, e.Potrazuje);
					Assert.Equal(stavka.Opis, e.Opis);
				});
		}

		[Fact]
		public void StornirajStavku_StavkaNull_Greska()
		{
			var nalog = GetNalog_SaViseStavki();

			Action storniraj = () => { nalog.StornirajStavku(null); };

			Assert.Throws<ArgumentNullException>(storniraj);
		}

		[Fact]
		public void StornirajStavku_NalogZakljucan_Greska()
		{
			var nalog = GetNalog_SaViseStavki();
			var stavka = nalog.Stavke.First();
			nalog.Primeni(new NalogZakljucan(nalog.Id));

			Action storniraj = () =>
			{
				nalog.StornirajStavku(stavka);
			};

			Assert.Throws<KnjizenjeException>(storniraj);
		}

		[Fact]
		public void StornirajStavku_StavkaNePripadaNalogu_Greska()
		{
			var nalog = GetNalog_SaViseStavki();
			var stavka = FinStavka.Nova(65, 9258, 0, null);

			Action storniraj = () =>
			{
				nalog.StornirajStavku(stavka);
			};

			Assert.Throws<KnjizenjeException>(storniraj);
		}

		[Fact]
		public void StornirajStavku_DodajeEvent()
		{
			var nalog = GetNalog_SaViseStavki();
			nalog.ClearUncommittedEvents();
			var stavka = nalog.Stavke.First();

			nalog.StornirajStavku(stavka);

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<StavkaProknjizena>(),
				e =>
				{
					Assert.Equal(nalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.NotEqual(stavka.Id, e.IdStavke);
					Assert.NotEqual(Guid.Empty, e.IdStavke);
					Assert.Equal(-stavka.Iznos.Duguje, e.Duguje);
					Assert.Equal(-stavka.Iznos.Potrazuje, e.Potrazuje);
					Assert.Null(e.Opis);
				});
		}

		[Fact]
		public void IzmeniZaglavlje_TipNull_Greska()
		{
			var nalog = GetNalog();

			Action izmeni = () =>
			{
				nalog.IzmeniZaglavlje(null, new DateTime(1987, 5, 2), "promenjen opis");
			};

			Assert.Throws<ArgumentNullException>(izmeni);
		}

		[Fact]
		public void IzmeniZaglavlje_NalogZakljucan_Greska()
		{
			var nalog = GetNalog();
			nalog.Primeni(new NalogZakljucan(nalog.Id));

			Action izmeni = () =>
			{
				nalog.IzmeniZaglavlje(TipNaloga.Izvodi,
				new DateTime(1987, 5, 2), "promenjen opis");
			};

			Assert.Throws<KnjizenjeException>(izmeni);
		}

		[Fact]
		public void IzmeniZaglavlje_DodajeEvent()
		{
			var nalog = GetNalog();

			nalog.IzmeniZaglavlje(TipNaloga.Izvodi, new DateTime(1987, 5, 2), "promenjen opis");

			Assert.Equal(1, nalog.UncommittedEvents.Count);
			Assert.Collection(nalog.UncommittedEvents.OfType<IzmenjenoZaglavljeNaloga>(),
				e =>
				{
					Assert.Equal(nalog.Id, e.IdNaloga);
					Assert.NotEqual(Guid.Empty, e.IdNaloga.Id);
					Assert.Equal(new DateTime(1987, 5, 2), e.DatumNaloga);
					Assert.Equal(TipNaloga.Izvodi.Id, e.IdTip);
					Assert.Equal("promenjen opis", e.Opis);
				});
		}

		private FinNalog GetNalog()
		{
			var nalog = GetPodrazumevaniNalog(new[]
			{
				FinStavka.Nova(1, 100, 0, "opis test stavke1"),
			});
			nalog.ClearUncommittedEvents();
			return nalog;
		}

		private FinNalog GetNalog_SaViseStavki()
		{
			var nalog = GetNalog();
			nalog.Primeni(new StavkaProknjizena(nalog.Id, Guid.NewGuid(),
				new DateTime(2018, 10, 1), 3, 0, 250, "opis"));
			nalog.Primeni(new StavkaProknjizena(nalog.Id, Guid.NewGuid(),
				new DateTime(2018, 10, 1), 3, 400, 0, "opis"));
			return nalog;

		}

		private FinNalog GetPodrazumevaniNalog(IEnumerable<FinStavka> stavke)
		{
			return FinNalog.OtvoriNovi(PodrazumevaniTipNaloga, PodrazumevaniDatumNaloga,
				PodrazumevaniOpisNaloga, stavke);

		}
	}
}
