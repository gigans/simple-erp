﻿using Microsoft.EntityFrameworkCore;
using ERP.SPA.Application.Data.EntityFramework;
using ERP.SPA.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Data.Repositories
{
	public interface ITipNalogaRepository
	{
		Task<IList<TipNaloga>> GetAllAsync();
	}

    public class TipNalogaRepository : ITipNalogaRepository
	{
		private readonly UIContext context;

		public TipNalogaRepository(UIContext context)
		{
			this.context = context;
		}

		public async Task<IList<TipNaloga>> GetAllAsync()
		{
			return await context.TipoviNaloga.ToListAsync();
		}
	}
}
