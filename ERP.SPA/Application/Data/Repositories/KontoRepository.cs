﻿using Microsoft.EntityFrameworkCore;
using ERP.SPA.Application.Data.EntityFramework;
using ERP.SPA.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Data.Repositories
{
	public interface IKontoRepository
	{
		Task<IList<Konto>> GetAllAsync();
	}

    public class KontoRepository : IKontoRepository
	{
		private readonly UIContext context;

		public KontoRepository(UIContext context)
		{
			this.context = context;
		}

		public async Task<IList<Konto>> GetAllAsync()
		{
			return await context.Konta.ToListAsync();
		}
    }
}
