﻿using Microsoft.EntityFrameworkCore;
using ERP.SPA.Application.Data.EntityFramework.Configurations;
using ERP.SPA.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Data.EntityFramework
{
    public class UIContext : DbContext
	{
		public DbSet<Konto> Konta { get; set; }
		public DbSet<TipNaloga> TipoviNaloga { get; set; }

		public UIContext()
		{
			
		}

		public UIContext(DbContextOptions options) : base(options)
		{
			//Database.EnsureDeleted();
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new KontoConfiguration());
			modelBuilder.ApplyConfiguration(new TipNalogaConfiguration());
		}

		public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await SaveChangesAsync();
			return true;
		}
	}
}
