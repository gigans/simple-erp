﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ERP.SPA.Application.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Data.EntityFramework
{
	public class UIContextSeed
	{
		public void Seed(UIContext context)
		{
			//if(!context.Database.IsInMemory())
			//	context.Database.Migrate();

			if (!context.TipoviNaloga.Any())
			{
				context.TipoviNaloga.AddRange(new[]
				{
					new TipNaloga() { Id = 1, Naziv="Ulazne fakture" },
					new TipNaloga() { Id = 2, Naziv="Izvodi" },
				});
			}
			if (!context.Konta.Any())
			{
				context.Konta.AddRange(new[]
				{
					new Konto() { Sifra = "435", Naziv = "zaduzenje" },
					new Konto() { Sifra = "241", Naziv = "tekuci racun" },
				});
			}

			context.SaveChanges();
		}
	}
}
