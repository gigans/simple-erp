﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ERP.SPA.Application.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.SPA.Application.Data.EntityFramework.Configurations
{
	class KontoConfiguration : IEntityTypeConfiguration<Konto>
	{
		public void Configure(EntityTypeBuilder<Konto> builder)
		{
			builder.ToTable("konto");
		}
	}
}
