﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ERP.SPA.Application.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ERP.SPA.Application.Data.EntityFramework.Configurations
{
	class TipNalogaConfiguration : IEntityTypeConfiguration<TipNaloga>
	{
		public void Configure(EntityTypeBuilder<TipNaloga> builder)
		{
			builder.ToTable("tip_naloga");

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).ValueGeneratedNever();
		}
	}
}
