﻿using Integration.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Commands
{
    public class IzmeniZaglavljeNalogaMessage: IIzmeniZaglavljeNalogaMessage
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }
		public long IdTip { get; }
		public DateTime DatumNaloga { get; }
		public string Opis { get; }

		public IzmeniZaglavljeNalogaMessage(Guid commandId, Guid idNaloga,
			long idTip, DateTime datumNaloga, string opis)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
			this.IdTip = idTip;
			this.DatumNaloga = datumNaloga;
			this.Opis = opis;
		}
    }
}
