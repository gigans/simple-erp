﻿using Integration.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Commands
{
    public class ObrisiNalogMessage : IObrisiNalogMessage
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }

		public ObrisiNalogMessage(Guid commandId, Guid idNaloga)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
		}
    }
}
