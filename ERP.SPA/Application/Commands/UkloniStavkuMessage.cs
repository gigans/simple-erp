﻿using Integration.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Commands
{
    public class UkloniStavkuMessage : IUkloniStavkuMessage
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }
		public Guid IdStavke { get; }

		public UkloniStavkuMessage(Guid commandId, Guid idNaloga, Guid idStavke)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
			this.IdStavke = idStavke;
		}
    }
}
