﻿using Integration.Contracts.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Commands
{
    public class OtvoriNalogMessage : IOtvoriNalogMessage
    {
		public Guid CommandId { get; }
		public long IdTip { get; }
		public DateTime DatumNaloga { get; }
		public string Opis { get; }
		public List<IStavka> Stavke { get; } = new List<IStavka>();

		public OtvoriNalogMessage(Guid commandId, long idTip, DateTime datumNaloga,
			string opis, IEnumerable<StavkaDTO> stavke)
		{
			this.CommandId = commandId;
			this.IdTip = idTip;
			this.DatumNaloga = datumNaloga;
			this.Opis = opis;
			this.Stavke.AddRange(stavke);
		}
	}
}
