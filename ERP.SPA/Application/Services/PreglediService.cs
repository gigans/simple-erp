﻿using ERP.SPA.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Services
{
	public interface IPreglediService
	{
		Task<IList<NalogGKDTO>> GetGlavnaKnjiga();
		Task<NalogFormDTO> GetNalogForm(Guid id);
		Task<IList<KarticaKontaDTO>> GetKarticaKonta(long id);
	}

	public class PreglediService : IPreglediService
    {
		private readonly HttpClient api;

		public PreglediService(HttpClient api)
		{
			this.api = api;
		}

		public async Task<IList<NalogGKDTO>> GetGlavnaKnjiga()
		{
			var response = await api.GetAsync("api/glavna-knjiga");
			response.EnsureSuccessStatusCode();
			var nalozi = await response.Content.ReadAsAsync<List<NalogGKDTO>>();
			return nalozi;
		}

		public async Task<NalogFormDTO> GetNalogForm(Guid id)
		{
			var response = await api.GetAsync($"api/nalog-form/{id}");
			response.EnsureSuccessStatusCode();
			var nalog = await response.Content.ReadAsAsync<NalogFormDTO>();
			foreach (var item in nalog.Stavke)
			{
				item.UBazi = true;
			}
			return nalog;
		}

		public async Task<IList<KarticaKontaDTO>> GetKarticaKonta(long id)
		{
			var response = await api.GetAsync($"api/kartica-konta/{id}");
			response.EnsureSuccessStatusCode();
			var kartica = await response.Content.ReadAsAsync<List<KarticaKontaDTO>>();
			return kartica;
		}
	}
}
