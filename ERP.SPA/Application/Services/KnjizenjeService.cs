﻿using ERP.SPA.Application.Commands;
using ERP.SPA.DTO;
using ERP.SPA.Infrastructure.Endpoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Application.Services
{
	public interface IKnjizenjeService
	{
		Task SnimiNovi(NalogFormDTO nalog);
		Task SnimiPromenu(NalogFormDTO nalog);
		Task Zakljucaj(Guid idNaloga);
		Task Otkljucaj(Guid idNaloga);
		Task Obrisi(Guid idNaloga);
	}

	public class KnjizenjeService : IKnjizenjeService
	{
		private readonly IKnjizenjeSendEndpoint queue;

		public KnjizenjeService(IKnjizenjeSendEndpoint queue)
		{
			this.queue = queue;
		}

		public async Task SnimiNovi(NalogFormDTO nalog)
		{
			var stavke = nalog.Stavke
				.Select(s => new StavkaDTO(s.IdKonto, s.Duguje, s.Potrazuje, s.Opis));
			var command = new OtvoriNalogMessage(Guid.NewGuid(), nalog.IdTip, nalog.Datum,
				nalog.Opis, stavke);
			await queue.Endpoint.Send(command);
		}

		public async Task SnimiPromenu(NalogFormDTO nalog)
		{
			Guid commandId = Guid.NewGuid();
			Guid idNaloga = nalog.Id.Value;
			var promeniZaglavlje = new IzmeniZaglavljeNalogaMessage(commandId,
				idNaloga, nalog.IdTip, nalog.Datum, nalog.Opis);
			await queue.Endpoint.Send(promeniZaglavlje);
			foreach (var item in nalog.Stavke.Where(x => !x.UBazi))
			{
				var prokjniziStavku = new ProknjiziStavkuMessage(commandId, idNaloga, item.IdKonto,
					item.Duguje, item.Potrazuje, item.Opis);
				await queue.Endpoint.Send(prokjniziStavku);
			}
			foreach (var item in nalog.Stavke.Where(x => x.UBazi && x.Stornirana))
			{
				var stornirajStavku = new StornirajStavkuMessage(commandId, idNaloga, item.Id);
				await queue.Endpoint.Send(stornirajStavku);
			}
			foreach (var item in nalog.Stavke.Where(x => x.UBazi && x.Uklonjena))
			{
				var ukloniStavku = new UkloniStavkuMessage(commandId, idNaloga, item.Id);
				await queue.Endpoint.Send(ukloniStavku);
			}
		}

		public async Task Zakljucaj(Guid idNaloga)
		{
			var command = new ZakljucajNalogMessage(Guid.NewGuid(), idNaloga);
			await queue.Endpoint.Send(command);
		}

		public async Task Otkljucaj(Guid idNaloga)
		{
			var command = new OtkljucajNalogMessage(Guid.NewGuid(), idNaloga);
			await queue.Endpoint.Send(command);
		}

		public async Task Obrisi(Guid idNaloga)
		{
			var command = new ObrisiNalogMessage(Guid.NewGuid(), idNaloga);
			await queue.Endpoint.Send(command);
		}
	}
}
