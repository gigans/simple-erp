﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.DTO
{
    public class KontoDTO
    {
		public long Id { get; set; }
		public string Sifra { get; set; }
		public string Naziv { get; set; }
	}
}
