﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using ERP.SPA.Application.Consumers;
using ERP.SPA.Infrastructure.Endpoints;
using GreenPipes;
using MassTransit;
using MassTransit.RabbitMqTransport;
using MassTransit.RabbitMqTransport.Configuration;
using MassTransit.Util;
using Microsoft.Extensions.Logging;
using Polly;

namespace ERP.SPA.Infrastructure.AutofacModules
{
	public class EventBusModule : Autofac.Module
	{
		private readonly string host;
		private readonly string userName;
		private readonly string password;
		private Policy busStartPolicy;

		public EventBusModule(string host, string userName, string password)
		{
			this.host = host;
			this.userName = userName;
			this.password = password;
			busStartPolicy = CreatePolicy();
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterConsumers(Assembly.GetExecutingAssembly());
			builder.Register(c => CreateAndStartBus(c))
				.As<IBusControl>()
				.As<IBus>()
				.SingleInstance()
				.AutoActivate();

			RegisterSendEndpoints(builder);
		}

		private void RegisterSendEndpoints(ContainerBuilder builder)
		{
			builder.Register<IKnjizenjeSendEndpoint>(c =>
				new KnjizenjeSendEndpoint(c.Resolve<IBusControl>()
					.GetSendEndpoint(new Uri($"rabbitmq://{host}/knjizenje_queue")).Result))
				.SingleInstance();
		}

		private void ConfigureHandlers(IRabbitMqBusFactoryConfigurator cfg, IComponentContext context)
		{
			cfg.ReceiveEndpoint("pregledi_queue", e =>
			{
				e.Consumer<GlavnaKnjigaConsumer>(context);
				e.Consumer<KnjizenjeErrorConsumer>(context);
			});
		}

		private IBusControl CreateAndStartBus(IComponentContext componentContext)
		{
			return busStartPolicy.Execute<IBusControl>(() =>
			{
				var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
				{
					var hostEndpoint = cfg.Host(new Uri($"rabbitmq://{host}"), h =>
					{
						h.Username(userName);
						h.Password(password);
					});
					ConfigureHandlers(cfg, componentContext);
				});
				var busHandle = TaskUtil.Await(() => busControl.StartAsync());
				return busControl;
			});
		}

		private Policy CreatePolicy()
		{
			return Policy
				.Handle<RabbitMqConnectionException>()
				.WaitAndRetry(5, retryAttempt =>
					TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
		}
	}
}
