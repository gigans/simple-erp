﻿using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.SPA.Infrastructure.Endpoints
{
	public interface IKnjizenjeSendEndpoint
	{
		ISendEndpoint Endpoint { get; }
	}

	public class KnjizenjeSendEndpoint : IKnjizenjeSendEndpoint
	{
		public ISendEndpoint Endpoint { get; }

		public KnjizenjeSendEndpoint(ISendEndpoint endpoint)
		{
			this.Endpoint = endpoint;
		}
	}
}
