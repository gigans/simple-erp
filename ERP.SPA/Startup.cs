﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using ERP.SPA.Application.Data.EntityFramework;
using ERP.SPA.Application.Data.Repositories;
using ERP.SPA.Application.Services;
using ERP.SPA.Hubs;
using ERP.SPA.Infrastructure.AutofacModules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Polly;
using Polly.Extensions.Http;

namespace ERP.SPA
{
	public class Startup
	{
		public Startup(IConfiguration configuration, IHostingEnvironment currentEnvironment)
		{
			Configuration = configuration;
			CurrentEnvironment = currentEnvironment;
		}

		public IConfiguration Configuration { get; }
		public IHostingEnvironment CurrentEnvironment { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			bool testing = CurrentEnvironment.IsEnvironment("Testing");
			services.AddMvc()
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());
			services.AddSignalR();
			services.AddCors(options => options.AddPolicy("CorsPolicy",
			builder =>
			{
				builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials();
			}));

			if (testing)
			{
				services.AddDbContext<UIContext>(options =>
					options.UseInMemoryDatabase("TestingDB"),
					ServiceLifetime.Scoped);
			}
			else
			{
				services.AddDbContext<UIContext>(options =>
				{
					options.UseNpgsql(Configuration["ConnectionString"], npgsqlOptionsAction: sqlOptions =>
					{
						sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
						sqlOptions.MigrationsAssembly(typeof(Startup).Assembly.FullName);
					});
				}, ServiceLifetime.Scoped);
			}
			AddApplicationServices(services);

			var container = new ContainerBuilder();
			container.Populate(services);
			container.RegisterModule(new EventBusModule(
				Configuration["EVENTBUS_HOST"],
				Configuration["EVENTBUS_USERNAME"],
				Configuration["EVENTBUS_PASSWORD"]));
			return new AutofacServiceProvider(container.Build());
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseStaticFiles();
			app.UseCookiePolicy();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});
			app.UseCors("CorsPolicy");
			app.UseSignalR(route =>
			{
				route.MapHub<GlavnaKnjigaHub>("/glavna-knjiga");
			});
		}

		private void AddApplicationServices(IServiceCollection services)
		{
			services.AddScoped<IKontoRepository, KontoRepository>();
			services.AddScoped<ITipNalogaRepository, TipNalogaRepository>();
			services.AddScoped<IKnjizenjeService, KnjizenjeService>();
			services.AddHttpClient<IPreglediService, PreglediService>(config =>
			{
				config.BaseAddress = new Uri(Configuration["PREGLEDI_HOST"]);
			})
			.AddPolicyHandler(GetRetryPolicy())
			.AddPolicyHandler(GetCircuitBreakerPolicy());
		}

		private IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
		{
			return HttpPolicyExtensions
				.HandleTransientHttpError()
				.OrResult(msg => msg.StatusCode == System.Net.HttpStatusCode.NotFound)
				.WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2,
					retryAttempt)));
		}

		private IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
		{
			return HttpPolicyExtensions
				.HandleTransientHttpError()
				.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30));
		}
	}
}
