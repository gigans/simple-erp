﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.SPA.Application.Data.Repositories;
using ERP.SPA.Application.Model;
using ERP.SPA.DTO;
using Microsoft.AspNetCore.Mvc;

namespace ERP.SPA.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TipoviNalogaController : ControllerBase
	{
		private readonly ITipNalogaRepository tipRepo;

		public TipoviNalogaController(ITipNalogaRepository tipRepo)
		{
			this.tipRepo = tipRepo;
		}

		[HttpGet]
		public async Task<IList<TipNaloga>> Get()
		{
			return await tipRepo.GetAllAsync();
		}
	}
}
