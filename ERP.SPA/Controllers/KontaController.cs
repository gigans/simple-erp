﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.SPA.Application.Data.Repositories;
using ERP.SPA.Application.Model;
using ERP.SPA.DTO;
using Microsoft.AspNetCore.Mvc;

namespace ERP.SPA.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class KontaController : ControllerBase
	{
		private readonly IKontoRepository kontoRepo;

		public KontaController(IKontoRepository kontoRepo)
		{
			this.kontoRepo = kontoRepo;
		}

		[HttpGet]
		public async Task<IList<Konto>> Get()
		{
			return await kontoRepo.GetAllAsync();
		}
	}
}
