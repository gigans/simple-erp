﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.SPA.Application.Services;
using ERP.SPA.DTO;
using Microsoft.AspNetCore.Mvc;

namespace ERP.SPA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NaloziController : ControllerBase
    {
		private readonly IPreglediService preglediSvc;
		private readonly IKnjizenjeService knjizenjeSvc;

		public NaloziController(IPreglediService preglediSvc, IKnjizenjeService knjizenjeSvc)
		{
			this.preglediSvc = preglediSvc;
			this.knjizenjeSvc = knjizenjeSvc;
		}

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] NalogFormDTO nalog)
        {
			await knjizenjeSvc.SnimiNovi(nalog);
			return Ok();
		}

		[HttpPut]
		public async Task<ActionResult> Put([FromBody] NalogFormDTO nalog)
		{
			await knjizenjeSvc.SnimiPromenu(nalog);
			return Ok();
		}

		[HttpPut("{id}/zakljucaj")]
		public async Task<ActionResult> Zakljucaj(Guid id)
		{
			await knjizenjeSvc.Zakljucaj(id);
			return Ok();
		}

		[HttpPut("{id}/otkljucaj")]
		public async Task<ActionResult> Otkljucaj(Guid id)
		{
			await knjizenjeSvc.Otkljucaj(id);
			return Ok();
		}

		[HttpDelete("{id}")]
		public async Task<ActionResult> Obrisi(Guid id)
		{
			await knjizenjeSvc.Obrisi(id);
			return Ok();
		}

		[HttpGet("promena/{id}")]
		public async Task<ActionResult<NalogFormDTO>> GetPromena(Guid id)
		{
			return Ok(await preglediSvc.GetNalogForm(id));
		}
	}
}
