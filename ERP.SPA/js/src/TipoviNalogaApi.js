import 'whatwg-fetch';
import { parseJson } from './utils'

const TipoviNalogaApi = {
  getTipovi: () => {
    return fetch(`/api/tipoviNaloga`, {
      method: 'GET'
    }).then(parseJson);
  }
}

export default TipoviNalogaApi;