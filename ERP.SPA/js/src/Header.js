import React from 'react'
import {
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import { Link } from 'react-router-dom'

const Header = () => (
  <Navbar color="primary" dark expand="md">
    <NavbarBrand tag={Link} to="/">Početna</NavbarBrand>
    <Collapse isOpen={true} navbar>
      <Nav className="ml-auto" navbar>
        <NavItem><NavLink tag={Link} to="/glavna-knjiga">Glavna knjiga</NavLink></NavItem>
        <NavItem><NavLink tag={Link} to="/kartica-konta">Kartica konta</NavLink></NavItem>
      </Nav>
    </Collapse>
  </Navbar>
)

export default Header
