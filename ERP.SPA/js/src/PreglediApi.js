import 'whatwg-fetch';
import { parseJson } from './utils'

const PreglediApi = {
  getGlavnaKnjiga: () => {
    return fetch(`/api/glavnaKnjiga`, {
      method: 'GET'
    }).then(parseJson)
  },
  getKarticaKonta: (id) => {
    return fetch(`/api/karticaKonta/${id}`, {
      method: 'GET'
    }).then(parseJson)
  },
}

export default PreglediApi;