import React, { Component } from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import { Link, Route } from 'react-router-dom'
import ObradaNaloga from './ObradaNaloga'
import Switch from 'react-router-dom/Switch';
import { Button } from 'reactstrap'
import { FaLockOpen, FaLock, FaEdit, FaPlus, FaTrashAlt } from 'react-icons/fa';
import NaloziApi from './NaloziApi';
import PreglediApi from './PreglediApi';
import { HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import showMessage from './showMessage';

class GlavnaKnjiga extends Component {

  constructor(props) {
    super(props);
    this.zakljucajNalog = this.zakljucajNalog.bind(this);
    this.otkljucajNalog = this.otkljucajNalog.bind(this);
    this.obrisiNalog = this.obrisiNalog.bind(this);
    this.initSignalr = this.initSignalr.bind(this);
    this.reloadData = this.reloadData.bind(this);
    this.state = { nalozi: [] };
  }

  componentWillMount() {
    this.reloadData();
  }

  componentDidMount() {
    this.initSignalr();
  }

  initSignalr() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl("glavna-knjiga")
      .configureLogging(LogLevel.Trace)
      .build();
    this.hubConnection.on('data-changed', () => {
      this.reloadData();
    });
    this.hubConnection.on('knjizenje-error', (error) => {
      let valErrors = error.validationErrors && 
        error.validationErrors.map(e => ` - ${e.message}`).join('\r\n');
      showMessage.error(error.message, 'Greška pri knjiženju', valErrors)
    });
    this.hubConnection.start()
      .catch(err => showMessage.error(`SignalR: ${err.message}`));
  }

  reloadData() {
    PreglediApi.getGlavnaKnjiga()
      .then(nalozi => this.setState({ nalozi: nalozi }));
  }

  zakljucajNalog(id) {
    NaloziApi.zakljucaj(id);
  }

  otkljucajNalog(id) {
    NaloziApi.otkljucaj(id);
  }

  obrisiNalog(id) {
    NaloziApi.obrisi(id);
  }

  render() {
    const cellStyle = {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center'
    }
    const iznosCellStyle = {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      textAlign: 'right'
    }

    const columns = [{
      Header: 'Datum naloga',
      accessor: 'Datum',
      style: cellStyle,
      Cell: props => <span>{new Date(props.value).toLocaleDateString('sr')}</span>
    }, {
      Header: 'Tip',
      accessor: 'TipNaziv',
      style: cellStyle
    }, {
      Header: 'Opis',
      accessor: 'Opis',
      style: cellStyle
    }, {
      Header: 'Broj stavki',
      accessor: 'BrojStavki',
      style: cellStyle
    }, {
      Header: 'Ukupno duguje',
      accessor: 'UkupnoDuguje',
      style: iznosCellStyle,
      Cell: props => <span>{props.value.toLocaleString('sr',
        { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
    }, {
      Header: 'Ukupno potražuje',
      accessor: 'UkupnoPotrazuje',
      style: iznosCellStyle,
      Cell: props => <span>{props.value.toLocaleString('sr',
        { minimumFractionDigits: 2, maximumFractionDigits: 2 })}</span>
    }, {
      id: 'Zakljucan',
      accessor: 'Zakljucan',
      style: cellStyle,
      Cell: props => props.value ?
        <Button onClick={() => this.otkljucajNalog(props.original.Id)}><FaLockOpen />Otključaj</Button> :
        <Button onClick={() => this.zakljucajNalog(props.original.Id)}><FaLock />Zaključaj</Button>
    }, {
      id: 'Promeni',
      accessor: 'Id',
      style: cellStyle,
      Cell: props => <Link to={`${this.props.match.url}/${props.value}`}>
        <Button disabled={props.original.Zakljucan}><FaEdit />Promeni</Button>
      </Link>
    }, {
      id: 'Obrisi',
      accessor: 'Id',
      style: cellStyle,
      Cell: props => 
        <Button color="danger" disabled={props.original.Zakljucan}
          onClick={() => this.obrisiNalog(props.original.Id)}><FaTrashAlt />Obriši</Button>
    }]

    return (
      <div>
        <h3 className='page-header'>Glavna knjiga</h3>
        <Link to={`${this.props.match.url}/novi-nalog`}>
          <Button color='success'><FaPlus />Novi nalog</Button>
        </Link>
        <ReactTable
          data={this.state.nalozi}
          columns={columns}
        />
        <Switch>
          <Route path={`${this.props.match.url}/novi-nalog`} component={ObradaNaloga} />
          <Route path={`${this.props.match.url}/:id`} component={ObradaNaloga} />
        </Switch>
      </div>
    );
  }
}

export default GlavnaKnjiga;