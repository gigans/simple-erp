import 'whatwg-fetch';
import { parseJson } from './utils'

const NaloziApi = {
  submitNalog: (nalog) => {
    return fetch('/api/nalozi', {
      method: nalog.Id ? 'PUT' : 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(nalog)
    })
  },

  getPromena: (id) => {
    return fetch(`/api/nalozi/promena/${id}`, {
      method: 'GET'
    }).then(parseJson)
  },

  getGlavnaKnjiga: () => {
    return fetch(`/api/nalozi/glavna-knjiga`, {
      method: 'GET'
    }).then(parseJson)
  },

  zakljucaj: (id) => {
    return fetch(`/api/nalozi/${id}/zakljucaj`, {
      method: 'PUT'
    });
  },

  otkljucaj: (id) => {
    return fetch(`/api/nalozi/${id}/otkljucaj`, {
      method: 'PUT'
    });
  },

  obrisi: (id) => {
    return fetch(`/api/nalozi/${id}`, {
      method: 'DELETE'
    });
  },
}

export default NaloziApi;