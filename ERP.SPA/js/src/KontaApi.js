import 'whatwg-fetch';
import { parseJson } from './utils'

const KontaApi = {
  getKonta: () => {
    return fetch(`/api/konta`, {
      method: 'GET'
    }).then(parseJson);
  }
}

export default KontaApi;