import showMessage from './showMessage'

const parseJson = (response) => {
  if (response.ok) {
    return response.json();
  }
  else {
    let error = `${response.status} ${response.statusText}`;
    showMessage.error(error, 'API greška', response.url);
  }
}

export { parseJson };