﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Knjizenje.API.Infrastructure.AutofacModules;
using Knjizenje.Infrastructure.Context;
using MassTransit;
using MassTransit.RabbitMqTransport;
using MassTransit.Util;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NLog;
using Polly;

namespace Knjizenje.API
{
	public class Startup
	{
		private readonly ILogger<Startup> logger;

		public Startup(IConfiguration configuration, IHostingEnvironment currentEnvironment, 
			ILogger<Startup> logger)
		{
			this.logger = logger;
			Configuration = configuration;
			CurrentEnvironment = currentEnvironment;
		}

		public IConfiguration Configuration { get; }
		public IHostingEnvironment CurrentEnvironment { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			bool testing = CurrentEnvironment.IsEnvironment("Testing");
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
			if (testing)
			{
				services.AddDbContext<KnjizenjeContext>(options =>
					options.UseInMemoryDatabase("TestingDB"));
			}
			else
			{
				GlobalDiagnosticsContext.Set("connectionString", Configuration["ConnectionString"]);
				services.AddDbContext<KnjizenjeContext>(options =>
				{
					options.UseNpgsql(Configuration["ConnectionString"], npgsqlOptionsAction: sqlOptions =>
					 {
						 sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
						 sqlOptions.MigrationsAssembly(typeof(Startup).Assembly.FullName);
					 });
				});
			}
			var container = new ContainerBuilder();
			container.Populate(services);

			container.RegisterModule(new ApplicationModule());
			container.RegisterModule(new EventStoreModule(
				Configuration["EVENTSTORE_HOST"],
				int.Parse(Configuration["EVENTSTORE_TCP_PORT"]),
				int.Parse(Configuration["EVENTSTORE_HTTP_PORT"]),
				logger));

			container.RegisterModule(new MediatrModule());
			container.RegisterModule(new EventBusModule(
				Configuration["EVENTBUS_HOST"],
				Configuration["EVENTBUS_USERNAME"],
				Configuration["EVENTBUS_PASSWORD"],
				logger));
			return new AutofacServiceProvider(container.Build());
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseMvc();
		}
	}
}
