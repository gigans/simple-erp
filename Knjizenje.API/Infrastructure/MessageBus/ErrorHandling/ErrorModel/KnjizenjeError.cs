﻿using Integration.Contracts.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Infrastructure.MessageBus.ErrorHandling.ErrorModel
{
	public class KnjizenjeError : IKnjizenjeError
	{
		public string Message { get; }
		public IEnumerable<IValidationError> ValidationErrors { get; }

		public KnjizenjeError(string message)
		{
			this.Message = message;
		}

		public KnjizenjeError(string message, IEnumerable<ValidationError> validationErrors)
			: this(message)
		{
			this.ValidationErrors = validationErrors;
		}
	}
}
