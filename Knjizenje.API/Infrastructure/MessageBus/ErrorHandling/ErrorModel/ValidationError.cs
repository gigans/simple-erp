﻿using Integration.Contracts.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Infrastructure.MessageBus.ErrorHandling.ErrorModel
{
    public class ValidationError : IValidationError
    {
		public string Property { get; }
		public string Message { get; }

		public ValidationError(string property, string message)
		{
			this.Property = property;
			this.Message = message;
		}
    }
}
