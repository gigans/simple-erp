﻿using GreenPipes;
using Knjizenje.API.Infrastructure.MessageBus.ErrorHandling;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Infrastructure.MessageBus
{
    public static class MessageBusExtensions
    {
		public static void UseConsumeExceptionHandler<T>(this IPipeConfigurator<T> configurator)
			where T : class, ConsumeContext
		{
			configurator.AddPipeSpecification(new ConsumeExceptionHandlerSpecification<T>());
		}
	}
}
