﻿using Autofac;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using Knjizenje.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Knjizenje.API.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<FinNalogRepository>().As<IFinNalogRepository>();
			builder.RegisterType<KontoRepository>().As<IKontoRepository>();
			builder.RegisterType<IzvodService>().As<IIzvodService>();	
		}
	}
}
