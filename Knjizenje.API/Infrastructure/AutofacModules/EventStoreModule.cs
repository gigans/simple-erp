﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Autofac;
using EventStore.ClientAPI;
using EventStore.ClientAPI.Projections;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Config;
using NLog.Targets;
using ES = Knjizenje.Infrastructure.EventSourcing;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Knjizenje.API.Infrastructure.AutofacModules
{
    public class EventStoreModule : Autofac.Module
	{
		private readonly ILogger logger;
		private readonly string host;
		private readonly int tcpPort;
		private readonly int httpPort;

		public EventStoreModule(string host, int tcpPort, int httpPort, ILogger logger)
		{
			this.host = host;
			this.tcpPort = tcpPort;
			this.httpPort = httpPort;
			this.logger = logger;
		}

		protected override void Load(ContainerBuilder builder)
		{
			logger.LogTrace($"Creating EventStore HTTP endpoint at {host}:{httpPort}");
			var httpEndpoint = new DnsEndPoint(host, httpPort);
			logger.LogTrace($"Connecting to EventStore at TCP endpoint {host}:{tcpPort}...");
			var esConnection = EventStoreConnection.Create(new Uri($"tcp://{host}:{tcpPort}"));
			esConnection.ConnectAsync().Wait();
			logger.LogTrace($"Successfully connected to EventStore at TCP endpoint {host}:{tcpPort}");
			builder.RegisterInstance(esConnection).As<IEventStoreConnection>();
			ProjectionsManager projectionsManager = new ProjectionsManager(esConnection.Settings.Log,
				httpEndpoint, TimeSpan.FromSeconds(5));
			builder.RegisterInstance(projectionsManager);
			builder.RegisterType<ES.EventStore>().As<ES.IEventStore>();
		}
	}
}
