﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using GreenPipes;
using Knjizenje.API.Application.EventBus;
using Knjizenje.API.Application.EventBus.IzmeniZaglavljeNaloga;
using Knjizenje.API.Application.EventBus.ObrisiNalog;
using Knjizenje.API.Application.EventBus.OtkljucajNalog;
using Knjizenje.API.Application.EventBus.OtvoriNalog;
using Knjizenje.API.Application.EventBus.ProknjiziIzvod;
using Knjizenje.API.Application.EventBus.ProknjiziStavku;
using Knjizenje.API.Application.EventBus.StornirajStavku;
using Knjizenje.API.Application.EventBus.UkloniStavku;
using Knjizenje.API.Application.EventBus.ZakljucajNalog;
using Knjizenje.API.Infrastructure.Data;
using Knjizenje.API.Infrastructure.MessageBus;
using Knjizenje.Infrastructure.Context;
using MassTransit;
using MassTransit.NLogIntegration;
using MassTransit.RabbitMqTransport;
using MassTransit.RabbitMqTransport.Configuration;
using MassTransit.Util;
using Microsoft.Extensions.Logging;
using Polly;

namespace Knjizenje.API.Infrastructure.AutofacModules
{
	public class EventBusModule : Autofac.Module
	{
		private readonly ILogger logger;
		private readonly string host;
		private readonly string userName;
		private readonly string password;
		private Policy busStartPolicy;

		public EventBusModule(string host, string userName, string password, ILogger logger)
		{
			this.logger = logger;
			this.host = host;
			this.userName = userName;
			this.password = password;
			busStartPolicy = CreatePolicy();
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterConsumers(Assembly.GetExecutingAssembly());
			builder.Register(c =>
			{
				//init database before subscription
				var context = c.Resolve<KnjizenjeContext>();
				KnjizenjeContextSeed seeder = new KnjizenjeContextSeed();
				seeder.Seed(context);

				return CreateAndStartBus(c);
			})
				.As<IBusControl>()
				.As<IBus>()
				.SingleInstance()
				.AutoActivate();
		}

		private void ConfigureHandlers(IRabbitMqHost host, IRabbitMqBusFactoryConfigurator cfg, 
			IComponentContext context)
		{
			cfg.ReceiveEndpoint(host, "knjizenje_queue", e =>
			{
				e.UseConcurrencyLimit(1);
				e.PrefetchCount = 1;
				e.Consumer<ProknjiziIzvodConsumer>(context);
				e.Consumer<IzmeniZaglavljeNalogaConsumer>(context);
				e.Consumer<OtkljucajNalogConsumer>(context);
				e.Consumer<OtvoriNalogConsumer>(context);
				e.Consumer<ProknjiziStavkuConsumer>(context);
				e.Consumer<StornirajStavkuConsumer>(context);
				e.Consumer<UkloniStavkuConsumer>(context);
				e.Consumer<ZakljucajNalogConsumer>(context);
				e.Consumer<ObrisiNalogConsumer>(context);
			});
		}

		private IBusControl CreateAndStartBus(IComponentContext componentContext)
		{
			return busStartPolicy.Execute<IBusControl>(() =>
			{
				var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
				{
					cfg.UseConsumeExceptionHandler();
					var loggerFactory = componentContext.Resolve<ILoggerFactory>();
					cfg.UseExtensionsLogging(loggerFactory);
					//cfg.UseNLog(NLog.LogManager.LogFactory);
					var hostEndpoint = cfg.Host(new Uri($"rabbitmq://{host}"), h =>
					{
						h.Username(userName);
						h.Password(password);
					});
					ConfigureHandlers(hostEndpoint, cfg, componentContext);
				});
				logger.LogTrace($"Connecting to host: {host}");
				var busHandle = TaskUtil.Await(() => busControl.StartAsync());
				return busControl;
			});
		}

		private Policy CreatePolicy()
		{
			return Policy
				.Handle<RabbitMqConnectionException>()
				.WaitAndRetry(5, retryAttempt =>
					TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
		}
	}
}
