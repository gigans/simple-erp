﻿namespace Knjizenje.API.Infrastructure.Data
{
	using Knjizenje.Domain.Entities.FinNalogAggregate;
	using Knjizenje.Domain.Entities.KontoAggregate;
	using Knjizenje.Infrastructure.Context;
	using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class KnjizenjeContextSeed
    {
        public void Seed(KnjizenjeContext context)
        {
			CreateLogTableIfNotExists(context);

			//if(!context.Database.IsInMemory())
			//	context.Database.Migrate();

			if (!context.TipoviNaloga.Any())
			{
				context.TipoviNaloga.AddRange(new[]
				{
					TipNaloga.UlazneFakture,
					TipNaloga.Izvodi
				});
			}
			if (!context.Konta.Any())
			{
				context.Konta.AddRange(new[]
				{
					new Konto("435", "zaduzenje"),
					new Konto("241", "tekuci racun")
				});
			}

			context.SaveChanges();
		}

		private void CreateLogTableIfNotExists(KnjizenjeContext context)
		{
			string command = $@"CREATE TABLE IF NOT EXISTS log
							( 
							ID serial primary key,
							entered_date date default Now(),
							log_application varchar(200) NULL, 
							log_date character varying(100) NULL, 
							log_level character varying(100) NULL, 
							log_logger character varying(8000) NULL, 
							log_message character varying(8000) NULL, 
							log_machine_name character varying(8000) NULL, 
							log_user_name character varying(8000) NULL, 
							log_call_site character varying(8000) NULL, 
							log_thread character varying(100) NULL, 
							log_exception character varying(8000) NULL, 
							log_stacktrace character varying(8000) NULL 
							)";
			context.Database.ExecuteSqlCommand(command);
		}
    }
}
