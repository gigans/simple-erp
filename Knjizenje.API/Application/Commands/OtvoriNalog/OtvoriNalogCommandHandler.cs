﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Exceptions;
using Knjizenje.Domain.SeedWork;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.OtvoriNalog
{
	public class OtvoriNalogCommandHandler : IRequestHandler<OtvoriNalogCommand, bool>
	{
		private readonly ILogger<OtvoriNalogCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public OtvoriNalogCommandHandler(IFinNalogRepository nalogRepo, ILogger<OtvoriNalogCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(OtvoriNalogCommand otvoriNalog, CancellationToken cancellationToken)
		{
			var tip = Enumeration.GetAll<TipNaloga>().Where(x => x.Id == otvoriNalog.IdTip).Single();
			var idPostojeceg = await nalogRepo.GetPostojeciAsync(tip, otvoriNalog.DatumNaloga);
			if (idPostojeceg != null)
				throw new KnjizenjeException("Nalog sa istim zaglavljem već postoji");

			var stavke = otvoriNalog.Stavke.Select(x => FinStavka.Nova(x.IdKonto, x.Duguje, x.Potrazuje, x.Opis));
			var nalog = FinNalog.OtvoriNovi(tip, otvoriNalog.DatumNaloga, otvoriNalog.Opis, stavke);
			await nalogRepo.SaveAsync(nalog, otvoriNalog.CommandId);

			return true;
		}
	}
}
