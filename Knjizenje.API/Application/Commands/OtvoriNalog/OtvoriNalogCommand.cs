﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.OtvoriNalog
{
	public class OtvoriNalogCommand : ICommand
	{
		public Guid CommandId { get; }
		public long IdTip { get; }
		public DateTime DatumNaloga { get; }
		public string Opis { get; }
		public List<StavkaDTO> Stavke { get; } = new List<StavkaDTO>();

		public OtvoriNalogCommand(Guid commandId, long idTip, DateTime datumNaloga, 
			string opis, IEnumerable<StavkaDTO> stavke)
		{
			this.CommandId = commandId;
			this.IdTip = idTip;
			this.DatumNaloga = datumNaloga;
			this.Opis = opis;
			this.Stavke.AddRange(stavke);
		}
	}
}
