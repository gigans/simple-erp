﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.OtvoriNalog
{
    public class StavkaDTO
    {
		public long IdKonto { get; set; }
		public decimal Duguje { get; set; }
		public decimal Potrazuje { get; set; }
		public string Opis { get; set; }

		public StavkaDTO(long idKonto, decimal duguje, decimal potrazuje, string opis)
		{
			this.IdKonto = idKonto;
			this.Duguje = duguje;
			this.Potrazuje = potrazuje;
			this.Opis = opis;
		}
	}
}
