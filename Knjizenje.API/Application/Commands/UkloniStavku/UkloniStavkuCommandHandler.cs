﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.UkloniStavku
{
	public class UkloniStavkuCommandHandler : IRequestHandler<UkloniStavkuCommand, bool>
	{
		private readonly ILogger<UkloniStavkuCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public UkloniStavkuCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<UkloniStavkuCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(UkloniStavkuCommand ukloniStavku, CancellationToken cancellationToken)
		{
			var nalog = await nalogRepo.GetAsync(new FinNalogId(ukloniStavku.IdNaloga.ToString()));
			var stavka = nalog.Stavke.Where(x => x.Id == ukloniStavku.IdStavke).SingleOrDefault();
			nalog.UkloniStavku(stavka);
			await nalogRepo.SaveAsync(nalog, ukloniStavku.CommandId);
			return true;
		}
	}
}
