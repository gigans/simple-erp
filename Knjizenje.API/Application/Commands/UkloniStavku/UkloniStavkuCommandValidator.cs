﻿using FluentValidation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.UkloniStavku
{
    public class UkloniStavkuCommandValidator : AbstractValidator<UkloniStavkuCommand>
	{
		public UkloniStavkuCommandValidator()
		{
			RuleFor(x => x.IdNaloga).NotEqual(Guid.Empty).WithMessage("IdNaloga je obavezan podatak");
			RuleFor(x => x.IdStavke).NotEqual(Guid.Empty).WithMessage("IdStavke je obavezan podatak");
		}
	}
}
