﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.StornirajStavku
{
    public class StornirajStavkuCommand : ICommand
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }
		public Guid IdStavke { get; }

		public StornirajStavkuCommand(Guid commandId, Guid idNaloga, Guid idStavke)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
			this.IdStavke = idStavke;
		}
    }
}
