﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.StornirajStavku
{
	public class StornirajStavkuCommandHandler : IRequestHandler<StornirajStavkuCommand, bool>
	{
		private readonly ILogger<StornirajStavkuCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public StornirajStavkuCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<StornirajStavkuCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(StornirajStavkuCommand stornirajStavku, CancellationToken cancellationToken)
		{
			var nalog = await nalogRepo.GetAsync(new FinNalogId(stornirajStavku.IdNaloga.ToString()));
			var stavka = nalog.Stavke.Where(x => x.Id == stornirajStavku.IdStavke).SingleOrDefault();
			nalog.StornirajStavku(stavka);
			await nalogRepo.SaveAsync(nalog, stornirajStavku.CommandId);
			return true;
		}
	}
}
