﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ObrisiNalog
{
	public class ObrisiNalogCommandHandler : IRequestHandler<ObrisiNalogCommand, bool>
	{
		private readonly ILogger<ObrisiNalogCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public ObrisiNalogCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<ObrisiNalogCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(ObrisiNalogCommand Obrisi, CancellationToken cancellationToken)
		{
			var nalog = await nalogRepo.GetAsync(new FinNalogId(Obrisi.IdNaloga.ToString()));
			nalog.Obrisi();
			await nalogRepo.SaveAsync(nalog, Obrisi.CommandId);
			return true;
		}
	}
}
