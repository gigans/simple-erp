﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ProknjiziStavku
{
	public class ProknjiziStavkuCommand : ICommand
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }
		public long IdKonto { get; }
		public decimal Duguje { get; }
		public decimal Potrazuje { get; }
		public string Opis { get; }

		public ProknjiziStavkuCommand(Guid commandId, Guid idNaloga, long idKonto, decimal duguje, decimal potrazuje,
			string opis)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
			this.IdKonto = idKonto;
			this.Duguje = duguje;
			this.Potrazuje = potrazuje;
			this.Opis = opis;
		}
	}
}
