﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ProknjiziStavku
{
	public class ProknjiziStavkuCommandHandler : IRequestHandler<ProknjiziStavkuCommand, bool>
	{
		private readonly ILogger<ProknjiziStavkuCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public ProknjiziStavkuCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<ProknjiziStavkuCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(ProknjiziStavkuCommand proknjiziStavku, CancellationToken cancellationToken)
		{
			var nalog = await nalogRepo.GetAsync(new FinNalogId(proknjiziStavku.IdNaloga.ToString()));
			var stavka = FinStavka.Nova(proknjiziStavku.IdKonto, proknjiziStavku.Duguje, proknjiziStavku.Potrazuje,
				proknjiziStavku.Opis);
			nalog.ProknjiziStavku(stavka);
			await nalogRepo.SaveAsync(nalog, proknjiziStavku.CommandId);
			return true;
		}
	}
}
