﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.OtkljucajNalog
{
	public class OtkljucajNalogCommandHandler : IRequestHandler<OtkljucajNalogCommand, bool>
	{
		private readonly ILogger<OtkljucajNalogCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public OtkljucajNalogCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<OtkljucajNalogCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(OtkljucajNalogCommand otkljucaj, CancellationToken cancellationToken)
		{
			var nalog = await nalogRepo.GetAsync(new FinNalogId(otkljucaj.IdNaloga.ToString()));
			nalog.Otkljucaj();
			await nalogRepo.SaveAsync(nalog, otkljucaj.CommandId);
			return true;
		}
	}
}
