﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.OtkljucajNalog
{
    public class OtkljucajNalogCommand : ICommand
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }

		public OtkljucajNalogCommand(Guid commandId, Guid idNaloga)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
		}
    }
}
