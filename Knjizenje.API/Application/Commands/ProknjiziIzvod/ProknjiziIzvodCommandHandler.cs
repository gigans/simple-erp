﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ProknjiziIzvod
{
	public class ProknjiziIzvodCommandHandler : IRequestHandler<ProknjiziIzvodCommand, bool>
	{
		private readonly ILogger<ProknjiziIzvodCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;
		private readonly IKontoRepository kontoRepo;
		private readonly IIzvodService izvodSvc;

		public ProknjiziIzvodCommandHandler(IFinNalogRepository nalogRepo,
			IKontoRepository kontoRepo, IIzvodService izvodSvc, ILogger<ProknjiziIzvodCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.kontoRepo = kontoRepo;
			this.izvodSvc = izvodSvc;
			this.logger = logger;
		}

		public async Task<bool> Handle(ProknjiziIzvodCommand izvod, CancellationToken cancellationToken)
		{
			logger.LogTrace($"Handling command {izvod.CommandId}");
			var idNaloga = await nalogRepo.GetPostojeciAsync(TipNaloga.Izvodi, izvod.Datum);

			var stavkeIzvoda = izvod.Stavke.Select(x => new StavkaIzvoda(x.SifraPlacanja, x.Duguje, x.Potrazuje));

			var stavke = await izvodSvc.FormirajStavkeNaloga(stavkeIzvoda);
			if (idNaloga != null)
			{
				logger.LogTrace("Postojeci nalog...");
				var nalog = await nalogRepo.GetAsync(idNaloga);

				foreach (var stavka in stavke)
				{
					nalog.ProknjiziStavku(stavka);
				}
				await nalogRepo.SaveAsync(nalog, izvod.CommandId);
			}
			else
			{
				logger.LogTrace("Novi nalog...");
				var nalog = FinNalog.OtvoriNovi(TipNaloga.Izvodi, izvod.Datum, null, stavke);
				await nalogRepo.SaveAsync(nalog, izvod.CommandId);
			}

			return true;
		}
	}
}
