﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ProknjiziIzvod
{
    public class StavkaIzvodaDTO
    {
		public int SifraPlacanja { get; set; }
		public decimal Duguje { get; set; }
		public decimal Potrazuje { get; set; }

		public StavkaIzvodaDTO(int sifraPlacanja, decimal duguje, decimal potrazuje)
		{
			this.SifraPlacanja = sifraPlacanja;
			this.Duguje = duguje;
			this.Potrazuje = potrazuje;
		}
	}
}
