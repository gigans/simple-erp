﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ProknjiziIzvod
{
    public class ProknjiziIzvodCommand : ICommand
	{
		public Guid CommandId { get; }
		public DateTime Datum { get; }
		public List<StavkaIzvodaDTO> Stavke { get; } = new List<StavkaIzvodaDTO>();

		public ProknjiziIzvodCommand(Guid commandId, DateTime datum, IEnumerable<StavkaIzvodaDTO> stavke)
		{
			this.CommandId = commandId;
			this.Datum = datum;
			this.Stavke.AddRange(stavke);
		}
    }
}
