﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.IzmeniZaglavljeNaloga
{
    public class IzmeniZaglavljeNalogaCommand : ICommand
	{
		public Guid CommandId { get; }
		public Guid IdNaloga { get; }
		public long IdTip { get; }
		public DateTime DatumNaloga { get; }
		public string Opis { get; }

		public IzmeniZaglavljeNalogaCommand(Guid commandId, Guid idNaloga,
			long idTip, DateTime datumNaloga, string opis)
		{
			this.CommandId = commandId;
			this.IdNaloga = idNaloga;
			this.IdTip = idTip;
			this.DatumNaloga = datumNaloga;
			this.Opis = opis;
		}
    }
}
