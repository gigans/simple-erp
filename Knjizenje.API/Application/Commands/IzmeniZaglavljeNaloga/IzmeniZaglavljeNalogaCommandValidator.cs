﻿using FluentValidation;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.SeedWork;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.IzmeniZaglavljeNaloga
{
    public class IzmeniZaglavljeNalogaCommandValidator : AbstractValidator<IzmeniZaglavljeNalogaCommand>
	{
		public IzmeniZaglavljeNalogaCommandValidator()
		{
			var sviTipovi = Enumeration.GetAll<TipNaloga>().Select(t => t.Id).ToList();
			RuleFor(x => x.IdNaloga).NotEqual(Guid.Empty).WithMessage("IdNaloga je obavezan podatak");
			RuleFor(x => x.DatumNaloga).GreaterThan(DateTime.MinValue).WithMessage("Datum naloga nije validan");
			RuleFor(x => x.IdTip).Must(x => sviTipovi.Contains(x)).WithMessage("Nepoznat tip naloga");
		}
	}
}
