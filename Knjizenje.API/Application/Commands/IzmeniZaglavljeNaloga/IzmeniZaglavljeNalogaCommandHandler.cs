﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Exceptions;
using Knjizenje.Domain.SeedWork;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.IzmeniZaglavljeNaloga
{
	public class IzmeniZaglavljeNalogaCommandHandler : IRequestHandler<IzmeniZaglavljeNalogaCommand, bool>
	{
		private readonly ILogger<IzmeniZaglavljeNalogaCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public IzmeniZaglavljeNalogaCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<IzmeniZaglavljeNalogaCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(IzmeniZaglavljeNalogaCommand izmeniZaglavlje, CancellationToken cancellationToken)
		{
			var idNaloga = new FinNalogId(izmeniZaglavlje.IdNaloga.ToString());
			var tip = Enumeration.GetAll<TipNaloga>().Where(x => x.Id == izmeniZaglavlje.IdTip).Single();
			var idPostojeceg = await nalogRepo.GetPostojeciAsync(tip, izmeniZaglavlje.DatumNaloga);
			if (idPostojeceg != null && idPostojeceg.Id != idNaloga.Id)
				throw new KnjizenjeException("Nalog sa istim zaglavljem već postoji");

			var zaglavlje = await nalogRepo.GetZaglavljeAsync(idNaloga);
			if (zaglavlje != null)
			{
				if (zaglavlje.DatumNaloga == izmeniZaglavlje.DatumNaloga &&
					zaglavlje.IdTip == izmeniZaglavlje.IdTip &&
					zaglavlje.Opis == izmeniZaglavlje.Opis)
					return true;

				var nalog = await nalogRepo.GetAsync(idNaloga);
				nalog.IzmeniZaglavlje(tip, izmeniZaglavlje.DatumNaloga, izmeniZaglavlje.Opis);
				await nalogRepo.SaveAsync(nalog, izmeniZaglavlje.CommandId);
			}
			return true;
		}
	}
}
