﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands
{
    public interface ICommand : IRequest<bool>
	{
		Guid CommandId { get; }
    }
}
