﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Services;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ZakljucajNalog
{
	public class ZakljucajNalogCommandHandler : IRequestHandler<ZakljucajNalogCommand, bool>
	{
		private readonly ILogger<ZakljucajNalogCommandHandler> logger;
		private readonly IFinNalogRepository nalogRepo;

		public ZakljucajNalogCommandHandler(IFinNalogRepository nalogRepo,
			ILogger<ZakljucajNalogCommandHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task<bool> Handle(ZakljucajNalogCommand zakljucaj, CancellationToken cancellationToken)
		{
			var nalog = await nalogRepo.GetAsync(new FinNalogId(zakljucaj.IdNaloga.ToString()));
			nalog.Zakljucaj();
			await nalogRepo.SaveAsync(nalog, zakljucaj.CommandId);
			return true;
		}
	}
}
