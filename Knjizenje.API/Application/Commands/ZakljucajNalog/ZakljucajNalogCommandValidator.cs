﻿using FluentValidation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.Commands.ZakljucajNalog
{
    public class ZakljucajNalogCommandValidator : AbstractValidator<ZakljucajNalogCommand>
	{
		public ZakljucajNalogCommandValidator()
		{
			RuleFor(x => x.IdNaloga).NotEqual(Guid.Empty).WithMessage("IdNaloga je obavezan podatak");
		}
	}
}
