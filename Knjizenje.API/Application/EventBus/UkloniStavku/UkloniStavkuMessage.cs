﻿using Knjizenje.API.Application.Commands.UkloniStavku;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.UkloniStavku
{
	public class UkloniStavkuMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
		public Guid IdStavke { get; set; }
	}
}
