﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.UkloniStavku;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.UkloniStavku
{
	public class UkloniStavkuConsumer : IConsumer<IUkloniStavkuMessage>
	{
		private readonly ILogger<UkloniStavkuConsumer> logger;
		private readonly IMediator mediator;

		public UkloniStavkuConsumer(IMediator mediator, ILogger<UkloniStavkuConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IUkloniStavkuMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new UkloniStavkuCommand(msg.CommandId, msg.IdNaloga, msg.IdStavke));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
