﻿using Knjizenje.API.Application.Commands.ProknjiziStavku;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ProknjiziStavku
{
	public class ProknjiziStavkuMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
		public long IdKonto { get; set; }
		public decimal Duguje { get; set; }
		public decimal Potrazuje { get; set; }
		public string Opis { get; set; }
	}
}
