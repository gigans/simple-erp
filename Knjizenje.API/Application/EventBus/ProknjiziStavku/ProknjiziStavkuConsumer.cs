﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.ProknjiziStavku;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ProknjiziStavku
{
	public class ProknjiziStavkuConsumer : IConsumer<IProknjiziStavkuMessage>
	{
		private readonly ILogger<ProknjiziStavkuConsumer> logger;
		private readonly IMediator mediator;

		public ProknjiziStavkuConsumer(IMediator mediator, ILogger<ProknjiziStavkuConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IProknjiziStavkuMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new ProknjiziStavkuCommand(msg.CommandId, msg.IdNaloga,
				msg.IdKonto, msg.Duguje, msg.Potrazuje, msg.Opis));
		}
	}
}
