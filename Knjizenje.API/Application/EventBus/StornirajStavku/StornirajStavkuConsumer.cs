﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.StornirajStavku;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.StornirajStavku
{
	public class StornirajStavkuConsumer : IConsumer<IStornirajStavkuMessage>
	{
		private readonly ILogger<StornirajStavkuConsumer> logger;
		private readonly IMediator mediator;

		public StornirajStavkuConsumer(IMediator mediator, ILogger<StornirajStavkuConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IStornirajStavkuMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new StornirajStavkuCommand(msg.CommandId, msg.IdNaloga, msg.IdStavke));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
