﻿using Knjizenje.API.Application.Commands.StornirajStavku;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.StornirajStavku
{
	public class StornirajStavkuMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
		public Guid IdStavke { get; set; }
	}
}
