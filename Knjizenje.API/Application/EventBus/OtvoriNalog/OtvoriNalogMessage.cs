﻿using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.OtvoriNalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.OtvoriNalog
{
	public class OtvoriNalogMessage : IOtvoriNalogMessage
	{
		public Guid CommandId { get; set; }
		public long IdTip { get; set; }
		public DateTime DatumNaloga { get; set; }
		public string Opis { get; set; }
		public List<IStavka> Stavke { get; set; }
	}
}
