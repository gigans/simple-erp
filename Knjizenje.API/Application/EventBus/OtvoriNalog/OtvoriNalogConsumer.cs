﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.OtvoriNalog;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.OtvoriNalog
{
	public class OtvoriNalogConsumer : IConsumer<IOtvoriNalogMessage>
	{
		private readonly ILogger<OtvoriNalogConsumer> logger;
		private readonly IMediator mediator;

		public OtvoriNalogConsumer(IMediator mediator, ILogger<OtvoriNalogConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IOtvoriNalogMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			var stavke = msg.Stavke.Select(s =>
				new StavkaDTO(s.IdKonto, s.Duguje, s.Potrazuje, s.Opis)).ToList();
			await mediator.Send(new OtvoriNalogCommand(msg.CommandId, msg.IdTip,
				msg.DatumNaloga, msg.Opis, stavke));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
