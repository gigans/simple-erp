﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus
{
    public class Response
    {
		public bool Success { get; }

		public Response(bool success)
		{
			this.Success = success;
		}
    }
}
