﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.IzmeniZaglavljeNaloga;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.IzmeniZaglavljeNaloga
{
	public class IzmeniZaglavljeNalogaConsumer : IConsumer<IIzmeniZaglavljeNalogaMessage>
	{
		private readonly ILogger<IzmeniZaglavljeNalogaConsumer> logger;
		private readonly IMediator mediator;

		public IzmeniZaglavljeNalogaConsumer(IMediator mediator, ILogger<IzmeniZaglavljeNalogaConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IIzmeniZaglavljeNalogaMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new IzmeniZaglavljeNalogaCommand(msg.CommandId,
				msg.IdNaloga, msg.IdTip, msg.DatumNaloga, msg.Opis));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
