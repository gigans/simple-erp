﻿using Knjizenje.API.Application.Commands.IzmeniZaglavljeNaloga;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.IzmeniZaglavljeNaloga
{
	public class IzmeniZaglavljeNalogaMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
		public long IdTip { get; set; }
		public DateTime DatumNaloga { get; set; }
		public string Opis { get; set; }
	}
}
