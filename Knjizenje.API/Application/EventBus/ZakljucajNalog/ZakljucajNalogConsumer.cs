﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.ZakljucajNalog;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ZakljucajNalog
{
	public class ZakljucajNalogConsumer : IConsumer<IZakljucajNalogMessage>
	{
		private readonly ILogger<ZakljucajNalogConsumer> logger;
		private readonly IMediator mediator;

		public ZakljucajNalogConsumer(IMediator mediator, ILogger<ZakljucajNalogConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IZakljucajNalogMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new ZakljucajNalogCommand(msg.CommandId, msg.IdNaloga));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
