﻿using Knjizenje.API.Application.Commands.ZakljucajNalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ZakljucajNalog
{
	public class ZakljucajNalogMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
	}
}
