﻿using Knjizenje.API.Application.Commands.OtkljucajNalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.OtkljucajNalog
{
	public class OtkljucajNalogMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
	}
}
