﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.OtkljucajNalog;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.OtkljucajNalog
{
	public class OtkljucajNalogConsumer : IConsumer<IOtkljucajNalogMessage>
	{
		private readonly ILogger<OtkljucajNalogConsumer> logger;
		private readonly IMediator mediator;

		public OtkljucajNalogConsumer(IMediator mediator, ILogger<OtkljucajNalogConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IOtkljucajNalogMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new OtkljucajNalogCommand(msg.CommandId, msg.IdNaloga));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
