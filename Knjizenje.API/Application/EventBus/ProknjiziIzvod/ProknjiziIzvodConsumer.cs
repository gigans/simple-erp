﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.ProknjiziIzvod;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ProknjiziIzvod
{
	public class ProknjiziIzvodConsumer : IConsumer<IProknjiziIzvodMessage>
	{
		private readonly ILogger<ProknjiziIzvodConsumer> logger;
		private readonly IMediator mediator;

		public ProknjiziIzvodConsumer(IMediator mediator, ILogger<ProknjiziIzvodConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IProknjiziIzvodMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			var stavke = msg.Stavke.Select(s =>
				new StavkaIzvodaDTO(s.SifraPlacanja, s.Duguje, s.Potrazuje)).ToList();
			await mediator.Send(new ProknjiziIzvodCommand(msg.CommandId, msg.Datum, stavke));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
