﻿using Knjizenje.API.Application.Commands.ProknjiziIzvod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ProknjiziIzvod
{
	public class ProknjiziIzvodMessage
	{
		public Guid CommandId { get; set; }
		public DateTime Datum { get; set; }
		public ICollection<StavkaIzvodaDTO> Stavke { get; set; }
	}
}
