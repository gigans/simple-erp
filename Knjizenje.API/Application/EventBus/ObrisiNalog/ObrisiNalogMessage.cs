﻿using Knjizenje.API.Application.Commands.ObrisiNalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ObrisiNalog
{
	public class ObrisiNalogMessage
	{
		public Guid CommandId { get; set; }
		public Guid IdNaloga { get; set; }
	}
}
