﻿using FluentValidation;
using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.ObrisiNalog;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Knjizenje.API.Application.EventBus.ObrisiNalog
{
	public class ObrisiNalogConsumer : IConsumer<IObrisiNalogMessage>
	{
		private readonly ILogger<ObrisiNalogConsumer> logger;
		private readonly IMediator mediator;

		public ObrisiNalogConsumer(IMediator mediator, ILogger<ObrisiNalogConsumer> logger)
		{
			this.mediator = mediator;
			this.logger = logger;
		}

		public async Task Consume(ConsumeContext<IObrisiNalogMessage> context)
		{
			logger.LogTrace($"Consuming command {context.Message.CommandId}");
			var msg = context.Message;
			await mediator.Send(new ObrisiNalogCommand(msg.CommandId, msg.IdNaloga));
			await context.RespondAsync(new Response(success: true));
		}
	}
}
