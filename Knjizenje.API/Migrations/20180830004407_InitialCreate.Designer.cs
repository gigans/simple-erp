﻿// <auto-generated />
using System;
using Knjizenje.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Knjizenje.API.Migrations
{
    [DbContext(typeof(KnjizenjeContext))]
    [Migration("20180830004407_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.2-rtm-30932")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("Knjizenje.Domain.Entities.FinNalogAggregate.FinNalog", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DatumNaloga");

                    b.Property<int>("Godina");

                    b.Property<long>("IdTip");

                    b.Property<string>("Opis");

                    b.Property<int>("RedniBroj");

                    b.Property<bool>("Zakljucan");

                    b.HasKey("Id");

                    b.HasIndex("IdTip", "DatumNaloga")
                        .IsUnique();

                    b.HasIndex("IdTip", "Godina", "RedniBroj")
                        .IsUnique();

                    b.ToTable("fin_nalog");
                });

            modelBuilder.Entity("Knjizenje.Domain.Entities.FinNalogAggregate.FinStavka", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DatumKnjizenja");

                    b.Property<long>("IdKonto");

                    b.Property<long?>("NalogId");

                    b.Property<string>("Opis");

                    b.HasKey("Id");

                    b.HasIndex("IdKonto");

                    b.HasIndex("NalogId");

                    b.ToTable("fin_stavka");
                });

            modelBuilder.Entity("Knjizenje.Domain.Entities.FinNalogAggregate.TipNaloga", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("tip_naloga");
                });

            modelBuilder.Entity("Knjizenje.Domain.Entities.Konto", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("konto");
                });

            modelBuilder.Entity("Knjizenje.Domain.Entities.FinNalogAggregate.FinNalog", b =>
                {
                    b.HasOne("Knjizenje.Domain.Entities.FinNalogAggregate.TipNaloga", "Tip")
                        .WithMany()
                        .HasForeignKey("IdTip")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.OwnsOne("Knjizenje.Domain.Entities.FinNalogAggregate.BilansNaloga", "Bilans", b1 =>
                        {
                            b1.Property<long?>("FinNalogId");

                            b1.Property<decimal>("Duguje")
                                .HasColumnName("Duguje");

                            b1.Property<decimal>("Potrazuje")
                                .HasColumnName("Potrazuje");

                            b1.ToTable("fin_nalog");

                            b1.HasOne("Knjizenje.Domain.Entities.FinNalogAggregate.FinNalog")
                                .WithOne("Bilans")
                                .HasForeignKey("Knjizenje.Domain.Entities.FinNalogAggregate.BilansNaloga", "FinNalogId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });

            modelBuilder.Entity("Knjizenje.Domain.Entities.FinNalogAggregate.FinStavka", b =>
                {
                    b.HasOne("Knjizenje.Domain.Entities.Konto", "Konto")
                        .WithMany()
                        .HasForeignKey("IdKonto")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Knjizenje.Domain.Entities.FinNalogAggregate.FinNalog", "Nalog")
                        .WithMany("Stavke")
                        .HasForeignKey("NalogId");

                    b.OwnsOne("Knjizenje.Domain.Entities.FinNalogAggregate.IznosStavke", "Iznos", b1 =>
                        {
                            b1.Property<long?>("FinStavkaId");

                            b1.Property<decimal>("Duguje")
                                .HasColumnName("Duguje");

                            b1.Property<decimal>("Potrazuje")
                                .HasColumnName("Potrazuje");

                            b1.ToTable("fin_stavka");

                            b1.HasOne("Knjizenje.Domain.Entities.FinNalogAggregate.FinStavka")
                                .WithOne("Iznos")
                                .HasForeignKey("Knjizenje.Domain.Entities.FinNalogAggregate.IznosStavke", "FinStavkaId")
                                .OnDelete(DeleteBehavior.Cascade);
                        });
                });
#pragma warning restore 612, 618
        }
    }
}
