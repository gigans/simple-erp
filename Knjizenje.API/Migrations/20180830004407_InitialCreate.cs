﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Knjizenje.API.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "konto",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_konto", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tip_naloga",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: true),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tip_naloga", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "fin_nalog",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IdTip = table.Column<long>(nullable: false),
                    RedniBroj = table.Column<int>(nullable: false),
                    DatumNaloga = table.Column<DateTime>(nullable: false),
                    Opis = table.Column<string>(nullable: true),
                    Duguje = table.Column<decimal>(nullable: false),
                    Potrazuje = table.Column<decimal>(nullable: false),
                    Zakljucan = table.Column<bool>(nullable: false),
                    Godina = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_fin_nalog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_fin_nalog_tip_naloga_IdTip",
                        column: x => x.IdTip,
                        principalTable: "tip_naloga",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "fin_stavka",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    NalogId = table.Column<long>(nullable: true),
                    IdKonto = table.Column<long>(nullable: false),
                    Duguje = table.Column<decimal>(nullable: false),
                    Potrazuje = table.Column<decimal>(nullable: false),
                    DatumKnjizenja = table.Column<DateTime>(nullable: false),
                    Opis = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_fin_stavka", x => x.Id);
                    table.ForeignKey(
                        name: "FK_fin_stavka_konto_IdKonto",
                        column: x => x.IdKonto,
                        principalTable: "konto",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_fin_stavka_fin_nalog_NalogId",
                        column: x => x.NalogId,
                        principalTable: "fin_nalog",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_fin_nalog_IdTip_DatumNaloga",
                table: "fin_nalog",
                columns: new[] { "IdTip", "DatumNaloga" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_fin_nalog_IdTip_Godina_RedniBroj",
                table: "fin_nalog",
                columns: new[] { "IdTip", "Godina", "RedniBroj" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_fin_stavka_IdKonto",
                table: "fin_stavka",
                column: "IdKonto");

            migrationBuilder.CreateIndex(
                name: "IX_fin_stavka_NalogId",
                table: "fin_stavka",
                column: "NalogId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "fin_stavka");

            migrationBuilder.DropTable(
                name: "konto");

            migrationBuilder.DropTable(
                name: "fin_nalog");

            migrationBuilder.DropTable(
                name: "tip_naloga");
        }
    }
}
