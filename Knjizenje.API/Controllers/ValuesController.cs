﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Knjizenje.API.Application.Commands.ProknjiziIzvod;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Knjizenje.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
		IMediator mediator;

		public ValuesController(IMediator mediator)
		{
			this.mediator = mediator;
		}
		// GET api/values
		//     [HttpGet]
		//     public ActionResult<IEnumerable<string>> Get()
		//     {
		//return new string[] { "value1", "value2" };
		//     }

		[HttpGet]
		public async Task<ActionResult<IEnumerable<string>>> Get()
		{
			var res = await mediator.Send(new ProknjiziIzvodCommand(Guid.NewGuid(),DateTime.Today, new[]
			{
				new StavkaIzvodaDTO
				{
					Duguje = 300,
					Potrazuje = 0,
					SifraPlacanja = 221
				}
			}));
			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		[HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

		// PUT api/values/5
		[HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
