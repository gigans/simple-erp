﻿using EventStore.ClientAPI.Projections;
using EventStore.ClientAPI.SystemData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Pregledi.API.Infrastructure.EventSourcing
{
    public static class EventStoreInitializer
    {
		public static async Task Initialize(ProjectionsManager projectionsManager, UserCredentials credentials)
		{
			var projections = await projectionsManager.ListAllAsync();
			var systemProjections = projections.Where(x => x.Name.StartsWith("$"));

			foreach (var item in systemProjections)
			{
				if(item.Status == "Stopped")
					await projectionsManager.EnableAsync(item.Name, credentials);
			}
			
			if (!projections.Any(x => x.Name == "Nalozi"))
			{
				string projectionFile = GetProjectionFile("Nalozi.js");
				await projectionsManager.CreateContinuousAsync("Nalozi", projectionFile, credentials);
				await projectionsManager.EnableAsync("Nalozi", credentials);
			}
		}


		private static string GetProjectionFile(string projection)
		{
			string projectionsDir = GetProjectionsDirectory();
			string path = Path.Combine(projectionsDir,
					"Nalozi.js");
			string[] lines = File.ReadAllLines(path);
			return string.Join(Environment.NewLine, lines);
		}

		private static string GetProjectionsDirectory()
		{
			return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
					@"Infrastructure/EventSourcing/Projections");
		}
	}
}
