﻿using Autofac;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Hadlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Pregledi.API.Infrastructure.AutofacModules
{
    public class MediatrModule : Autofac.Module
    {
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly)
				.AsImplementedInterfaces().InstancePerLifetimeScope();

			//builder.RegisterAssemblyTypes(typeof(NalogOtvoren).GetTypeInfo().Assembly)
			//	.AsClosedTypesOf(typeof(IRequestHandler<,>));

			builder.RegisterAssemblyTypes(typeof(GlavnaKnjigaNalogOtvorenHandler).GetTypeInfo().Assembly)
				.AsClosedTypesOf(typeof(INotificationHandler<>)).InstancePerLifetimeScope();

			builder.Register<ServiceFactory>(ctx =>
			{
				var c = ctx.Resolve<IComponentContext>();
				return t => c.Resolve(t);
			});
		}
	}
}
