﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using GreenPipes;
using MassTransit;
using MassTransit.RabbitMqTransport;
using MassTransit.Util;
using Microsoft.Extensions.Logging;
using Polly;
using Pregledi.API.Application.Data.EntityFramework;

namespace Pregledi.API.Infrastructure.AutofacModules
{
	public class EventBusModule : Autofac.Module
	{
		private readonly ILogger logger;
		private readonly string host;
		private readonly string userName;
		private readonly string password;
		private Policy busStartPolicy;

		public EventBusModule(string host, string userName, string password, ILogger logger)
		{
			this.logger = logger;
			this.host = host;
			this.userName = userName;
			this.password = password;
			busStartPolicy = CreatePolicy();
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterConsumers(Assembly.GetExecutingAssembly());
			builder.Register(c =>
			{
				//init database before subscription
				var context = c.Resolve<PreglediContext>();
				PreglediContextSeed seeder = new PreglediContextSeed();
				seeder.Seed(context);

				return CreateAndStartBus(c);
			})
				.As<IBusControl>()
				.As<IBus>()
				.SingleInstance()
				.AutoActivate();
		}

		private IBusControl CreateAndStartBus(IComponentContext componentContext)
		{
			return busStartPolicy.Execute<IBusControl>(() =>
			{
				var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
				{
					var hostEndpoint = cfg.Host(new Uri($"rabbitmq://{host}"), h =>
					{
						h.Username(userName);
						h.Password(password);
					});
				});
				logger.LogTrace($"Connecting to host: {host}");
				var busHandle = TaskUtil.Await(() => busControl.StartAsync());
				return busControl;
			});
		}

		private Policy CreatePolicy()
		{
			return Policy
				.Handle<RabbitMqConnectionException>()
				.WaitAndRetry(5, retryAttempt =>
					TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
		}
	}
}
