﻿using Autofac;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Pregledi.API.Infrastructure.AutofacModules
{
    public class ApplicationModule : Autofac.Module
    {
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<NotificationQueue>().As<INotificationQueue>().InstancePerLifetimeScope();
			builder.RegisterType<ProcessedEventRepository>().As<IProcessedEventRepository>().InstancePerLifetimeScope();
			builder.RegisterType<NalogGKRepository>().As<INalogGKRepository>().InstancePerLifetimeScope();
			builder.RegisterType<TipNalogaRepository>().As<ITipNalogaRepository>().InstancePerLifetimeScope();
			builder.RegisterType<NalogFormRepository>().As<INalogFormRepository>().InstancePerLifetimeScope();
			builder.RegisterType<StavkaFormRepository>().As<IStavkaFormRepository>().InstancePerLifetimeScope();
			builder.RegisterType<KontoRepository>().As<IKontoRepository>().InstancePerLifetimeScope();
			builder.RegisterType<KarticaKontaRepository>().As<IKarticaKontaRepository>().InstancePerLifetimeScope();
		}
	}
}
