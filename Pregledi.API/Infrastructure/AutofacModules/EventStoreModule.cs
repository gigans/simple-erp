﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using EventStore.ClientAPI;
using EventStore.ClientAPI.Projections;
using EventStore.ClientAPI.SystemData;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Config;
using NLog.Targets;
using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Infrastructure.EventSourcing;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace Pregledi.API.Infrastructure.AutofacModules
{
	public class EventStoreModule : Autofac.Module
	{
		private readonly ILogger logger;
		private readonly string host;
		private readonly int tcpPort;
		private readonly int httpPort;
		private readonly string username;
		private readonly string password;

		public EventStoreModule(string host, int tcpPort, int httpPort, string username, string password,
			ILogger logger)
		{
			this.host = host;
			this.tcpPort = tcpPort;
			this.httpPort = httpPort;
			this.logger = logger;
			this.username = username;
			this.password = password;
		}

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<EventResolver>().As<IEventResolver>().InstancePerLifetimeScope();

			var httpEndpoint = new DnsEndPoint(host, httpPort);
			ProjectionsManager projectionsManager = new ProjectionsManager(ConnectionSettings.Default.Log, httpEndpoint, TimeSpan.FromSeconds(5));
			builder.RegisterInstance(projectionsManager);
			builder.Register(c =>
			{
				//init database before subscription
				var context = c.Resolve<PreglediContext>();
				PreglediContextSeed seeder = new PreglediContextSeed();
				seeder.Seed(context);

				//init projections
				var projectionManager = c.Resolve<ProjectionsManager>();
				var env = c.Resolve<IConfiguration>();
				var credentials = new UserCredentials(env["EVENTSTORE_USERNAME"], env["EVENTSTORE_PASSWORD"]);
				EventStoreInitializer.Initialize(projectionManager, credentials).Wait();

				logger.LogDebug($"Connecting to EventStore at TCP endpoint {host}:{tcpPort}...");
				var esConnection = EventStoreConnection.Create(new Uri($"tcp://{host}:{tcpPort}"));
				esConnection.Connected += (o, args) => logger.LogDebug("Connected");
				esConnection.Disconnected += (o, args) => logger.LogDebug("Disconnected");
				esConnection.Reconnecting += (o, args) => logger.LogDebug("Reconnecting");
				esConnection.Closed += (o, args) => logger.LogDebug("Closed");
				esConnection.ErrorOccurred += (o, args) => logger.LogDebug($"ErrorOccurred {args.Exception.Message}");
				esConnection.ConnectAsync().Wait();
				logger.LogDebug($"Successfully connected to EventStore at TCP endpoint {host}:{tcpPort}");

				var catchUpSettings = new CatchUpSubscriptionSettings(100, 100, true, true);
				var freshContext = c.Resolve<IComponentContext>();
				long? lastPosition;
				string stream = "nalozi";
				using (var scope = freshContext.Resolve<ILifetimeScope>().BeginLifetimeScope())
				{
					var processedRepo = scope.Resolve<IProcessedEventRepository>();
					var lastEvent = processedRepo.GetLastProcessed(stream);
					if (lastEvent != null)
						lastPosition = lastEvent.Checkpoint;
					else
						lastPosition = null;
				}
				esConnection.SubscribeToStreamFrom(stream, lastPosition, catchUpSettings,
					async (s, e) =>
					{
						using (var scope = freshContext.Resolve<ILifetimeScope>().BeginLifetimeScope())
						{
							var eventResolver = scope.Resolve<IEventResolver>();
							await eventResolver.ResolveAsync(e);
						}
					}, null, (s, reason, exc) =>
					{
						logger.LogDebug($"Dropped");
						logger.LogDebug(reason.ToString());
						logger.LogDebug(exc.Message, exc);
					}, new UserCredentials(username, password));
				return esConnection;
			})
			.As<IEventStoreConnection>()
			.SingleInstance()
			.AutoActivate();
		}
	}
}
