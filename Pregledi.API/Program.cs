﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EventStore.ClientAPI;
using EventStore.ClientAPI.Projections;
using EventStore.ClientAPI.SystemData;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;
using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Infrastructure.EventSourcing;

namespace Pregledi.API
{
    public class Program
    {
		public static void Main(string[] args)
		{
			// NLog: setup the logger first to catch all errors
			var logger = NLog.Web.NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();
			try
			{
				var host = CreateWebHostBuilder(args).Build();
				InitializeDatabase(host);
				host.Run();
			}
			catch (Exception ex)
			{
				//NLog: catch setup errors
				logger.Error(ex, "Stopped program because of exception");
				throw;
			}
			finally
			{
				// Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
				NLog.LogManager.Shutdown();
			}
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>

			WebHost.CreateDefaultBuilder(args)
				.ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.SetMinimumLevel(LogLevel.Trace);
				})
				.UseNLog()
				.UseStartup<Startup>();

		public static void InitializeDatabase(IWebHost host)
		{
			using (var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
				using (var context = services.GetRequiredService<PreglediContext>())
				{
					var seeder = new PreglediContextSeed();
					seeder.Seed(context);
				}
			}
		}

		public static void InitializeEventStore(IWebHost host)
		{
			using (var scope = host.Services.CreateScope())
			{
				var services = scope.ServiceProvider;
				var projectionManager = services.GetRequiredService<ProjectionsManager>();
				var env = services.GetRequiredService<IConfiguration>();
				var credentials = new UserCredentials(env["EVENTSTORE_USERNAME"], env["EVENTSTORE_PASSWORD"]);
				EventStoreInitializer.Initialize(projectionManager, credentials).Wait();
			}
		}
	}
}
