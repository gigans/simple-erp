﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Handlers
{
    public class StavkaFormStavkaUklonjenaHandler : INotificationHandler<StavkaUklonjena>
	{
		private readonly ILogger<StavkaFormStavkaUklonjenaHandler> logger;
		private readonly IStavkaFormRepository stavkaRepo;

		public StavkaFormStavkaUklonjenaHandler(IStavkaFormRepository stavkaRepo,
			ILogger<StavkaFormStavkaUklonjenaHandler> logger)
		{
			this.stavkaRepo = stavkaRepo;
			this.logger = logger;
		}

		public async Task Handle(StavkaUklonjena stavkaUklonjena, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling StavkaFormStavkaUklonjenaHandler: {stavkaUklonjena.IdStavke}");
			var stavka = await stavkaRepo.GetAsync(stavkaUklonjena.IdStavke);
			stavkaRepo.Remove(stavka);
		}
	}
}
