﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaNalogOtkljucanHandler : INotificationHandler<NalogOtkljucan>
	{
		private readonly ILogger<GlavnaKnjigaNalogOtkljucanHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaNalogOtkljucanHandler(INalogGKRepository nalogRepo,
			INotificationQueue notifications,
			ILogger<GlavnaKnjigaNalogOtkljucanHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(NalogOtkljucan nalogOtkljucan, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaNalogOtkljucanHandler: {nalogOtkljucan.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(nalogOtkljucan.IdNaloga);
			if (nalogOtkljucan.EventNumber > nalog.Version)
			{
				nalog.Version = nalogOtkljucan.EventNumber;
				nalog.Zakljucan = false;
				notifications.Add(new GlavnaKnjigaChanged(nalogOtkljucan.EventId));
			}
		}
	}
}
