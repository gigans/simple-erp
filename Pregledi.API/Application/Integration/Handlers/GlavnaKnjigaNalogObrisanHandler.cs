﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaNalogObrisanHandler : INotificationHandler<NalogObrisan>
	{
		private readonly ILogger<GlavnaKnjigaNalogObrisanHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaNalogObrisanHandler(INalogGKRepository nalogRepo,
			INotificationQueue notifications,
			ILogger<GlavnaKnjigaNalogObrisanHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(NalogObrisan nalogObrisan, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaNalogObrisanHandler: {nalogObrisan.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(nalogObrisan.IdNaloga);
			if (nalogObrisan.EventNumber > nalog.Version)
			{
				nalogRepo.Remove(nalog);
				notifications.Add(new GlavnaKnjigaChanged(nalogObrisan.EventId));
			}
		}
	}
}
