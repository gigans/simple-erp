﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaIzmenjenoZaglavljeNalogaHandler : INotificationHandler<IzmenjenoZaglavljeNaloga>
	{
		private readonly ILogger<GlavnaKnjigaIzmenjenoZaglavljeNalogaHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly ITipNalogaRepository tipNalogaRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaIzmenjenoZaglavljeNalogaHandler(INalogGKRepository nalogRepo, 
			ITipNalogaRepository tipNalogaRepo, INotificationQueue notifications,
			ILogger<GlavnaKnjigaIzmenjenoZaglavljeNalogaHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.tipNalogaRepo = tipNalogaRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(IzmenjenoZaglavljeNaloga izmenjenoZaglavlje, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaIzmenjenoZaglavljeNalogaHandler: {izmenjenoZaglavlje.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(izmenjenoZaglavlje.IdNaloga);
			if (izmenjenoZaglavlje.EventNumber > nalog.Version)
			{
				nalog.Version = izmenjenoZaglavlje.EventNumber;
				string tipNalogaNaziv = await tipNalogaRepo.GetNazivAsync(izmenjenoZaglavlje.IdTip);
				nalog.Datum = izmenjenoZaglavlje.DatumNaloga;
				nalog.TipNaziv = tipNalogaNaziv;
				nalog.Opis = izmenjenoZaglavlje.Opis;
				notifications.Add(new GlavnaKnjigaChanged(izmenjenoZaglavlje.EventId));
			}
		}
	}
}
