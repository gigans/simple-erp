﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaNalogZakljucanHandler : INotificationHandler<NalogZakljucan>
	{
		private readonly ILogger<GlavnaKnjigaNalogZakljucanHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaNalogZakljucanHandler(INalogGKRepository nalogRepo, INotificationQueue notifications,
			ILogger<GlavnaKnjigaNalogZakljucanHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(NalogZakljucan nalogZakljucan, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaNalogZakljucanHandler: {nalogZakljucan.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(nalogZakljucan.IdNaloga);
			if (nalogZakljucan.EventNumber > nalog.Version)
			{
				nalog.Version = nalogZakljucan.EventNumber;
				nalog.Zakljucan = true;
				notifications.Add(new GlavnaKnjigaChanged(nalogZakljucan.EventId));
			}
		}
	}
}
