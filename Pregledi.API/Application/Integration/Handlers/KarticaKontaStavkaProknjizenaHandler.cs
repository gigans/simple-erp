﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Handlers
{
    public class KarticaKontaStavkaProknjizenaHandler : INotificationHandler<StavkaProknjizena>
	{
		private readonly ILogger<StavkaFormStavkaProknjizenaHandler> logger;
		private readonly IKarticaKontaRepository karticaRepo;
		private readonly INalogGKRepository nalogRepo;
		private readonly IKontoRepository kontoRepo;

		public KarticaKontaStavkaProknjizenaHandler(IKarticaKontaRepository karticaRepo,
			IKontoRepository kontoRepo, INalogGKRepository nalogRepo,
			ILogger<StavkaFormStavkaProknjizenaHandler> logger)
		{
			this.karticaRepo = karticaRepo;
			this.kontoRepo = kontoRepo;
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task Handle(StavkaProknjizena stavkaProknjizena, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling KarticaKontaStavkaProknjizenaHandler: {stavkaProknjizena.IdStavke}");
			var konto = await kontoRepo.GetAsync(stavkaProknjizena.IdKonto);
			var nalog = await nalogRepo.GetAsync(stavkaProknjizena.IdNaloga);
			KarticaKonta stavkaKartice = new KarticaKonta()
			{
				Id = stavkaProknjizena.IdStavke,
				IdNaloga = stavkaProknjizena.IdNaloga,
				DatumKnjizenja = stavkaProknjizena.DatumKnjizenja,
				DatumNaloga = nalog.Datum,
				IdKonto = stavkaProknjizena.IdKonto,
				Konto = konto.Sifra,
				Opis = stavkaProknjizena.Opis,
				TipNaloga = nalog.TipNaziv,
				Duguje = stavkaProknjizena.Duguje,
				Potrazuje = stavkaProknjizena.Potrazuje,
				Saldo = stavkaProknjizena.Duguje - stavkaProknjizena.Potrazuje,
				Created = stavkaProknjizena.Created
			};
			await karticaRepo.AddAsync(stavkaKartice);
		}
	}
}
