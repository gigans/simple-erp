﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class NalogFormNalogObrisanHandler : INotificationHandler<NalogObrisan>
	{
		private readonly ILogger<NalogFormNalogObrisanHandler> logger;
		private readonly INalogFormRepository nalogRepo;

		public NalogFormNalogObrisanHandler(INalogFormRepository nalogRepo,
			ILogger<NalogFormNalogObrisanHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task Handle(NalogObrisan nalogObrisan, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling NalogFormNalogObrisanHandler: {nalogObrisan.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(nalogObrisan.IdNaloga);
			if (nalogObrisan.EventNumber > nalog.Version)
			{
				nalogRepo.Remove(nalog);
			}
		}
	}
}
