﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class NalogFormIzmenjenoZaglavljeNalogaHandler : INotificationHandler<IzmenjenoZaglavljeNaloga>
	{
		private readonly ILogger<NalogFormIzmenjenoZaglavljeNalogaHandler> logger;
		private readonly INalogFormRepository nalogRepo;

		public NalogFormIzmenjenoZaglavljeNalogaHandler(INalogFormRepository nalogRepo, 
			ILogger<NalogFormIzmenjenoZaglavljeNalogaHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public async Task Handle(IzmenjenoZaglavljeNaloga izmenjenoZaglavlje, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling NalogFormIzmenjenoZaglavljeNalogaHandler: {izmenjenoZaglavlje.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(izmenjenoZaglavlje.IdNaloga);
			if (izmenjenoZaglavlje.EventNumber > nalog.Version)
			{
				nalog.Version = izmenjenoZaglavlje.EventNumber;
				nalog.Datum = izmenjenoZaglavlje.DatumNaloga;
				nalog.IdTip = izmenjenoZaglavlje.IdTip;
				nalog.Opis = izmenjenoZaglavlje.Opis;
			}
		}
	}
}
