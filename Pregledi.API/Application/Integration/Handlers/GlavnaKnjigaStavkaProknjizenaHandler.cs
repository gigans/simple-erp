﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaStavkaProknjizenaHandler : INotificationHandler<StavkaProknjizena>
	{
		private readonly ILogger<GlavnaKnjigaStavkaProknjizenaHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaStavkaProknjizenaHandler(INalogGKRepository nalogRepo,
			INotificationQueue notifications,
			ILogger<GlavnaKnjigaStavkaProknjizenaHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(StavkaProknjizena stavkaProknjizena, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaStavkaProknjizenaHandler: {stavkaProknjizena.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(stavkaProknjizena.IdNaloga);
			if (stavkaProknjizena.EventNumber > nalog.Version)
			{
				nalog.Version = stavkaProknjizena.EventNumber;
				nalog.BrojStavki++;
				nalog.UkupnoDuguje += stavkaProknjizena.Duguje;
				nalog.UkupnoPotrazuje += stavkaProknjizena.Potrazuje;
				nalog.UkupnoSaldo = nalog.UkupnoDuguje - nalog.UkupnoPotrazuje;
				notifications.Add(new GlavnaKnjigaChanged(stavkaProknjizena.EventId));
			}	
		}
	}
}
