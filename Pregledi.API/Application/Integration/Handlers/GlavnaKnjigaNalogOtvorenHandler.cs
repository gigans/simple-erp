﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaNalogOtvorenHandler : INotificationHandler<NalogOtvoren>
	{
		private readonly ILogger<GlavnaKnjigaNalogOtvorenHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly ITipNalogaRepository tipNalogaRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaNalogOtvorenHandler(INalogGKRepository nalogRepo,
			ITipNalogaRepository tipNalogaRepo, INotificationQueue notifications,
			ILogger<GlavnaKnjigaNalogOtvorenHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.tipNalogaRepo = tipNalogaRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(NalogOtvoren nalogOtvoren, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaNalogOtvorenHandler: {nalogOtvoren.IdNaloga}");
			string tipNalogaNaziv = await tipNalogaRepo.GetNazivAsync(nalogOtvoren.IdTip);
			NalogGlavnaKnjiga nalog = new NalogGlavnaKnjiga()
			{
				Id = nalogOtvoren.IdNaloga,
				Datum = nalogOtvoren.DatumNaloga,
				TipNaziv = tipNalogaNaziv,
				Opis = nalogOtvoren.Opis,
				BrojStavki = 0,
				UkupnoDuguje = 0,
				UkupnoPotrazuje = 0,
				UkupnoSaldo = 0,
				Zakljucan = false
			};
			nalogRepo.Add(nalog);
			notifications.Add(new GlavnaKnjigaChanged(nalogOtvoren.EventId));
		}
	}
}
