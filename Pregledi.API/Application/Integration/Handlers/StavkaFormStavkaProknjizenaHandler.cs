﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Handlers
{
    public class StavkaFormStavkaProknjizenaHandler : INotificationHandler<StavkaProknjizena>
	{
		private readonly ILogger<StavkaFormStavkaProknjizenaHandler> logger;
		private readonly IStavkaFormRepository stavkaRepo;
		private readonly IKontoRepository kontoRepo;

		public StavkaFormStavkaProknjizenaHandler(IStavkaFormRepository stavkaRepo,
			IKontoRepository kontoRepo,
			ILogger<StavkaFormStavkaProknjizenaHandler> logger)
		{
			this.stavkaRepo = stavkaRepo;
			this.kontoRepo = kontoRepo;
			this.logger = logger;
		}

		public async Task Handle(StavkaProknjizena stavkaProknjizena, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling StavkaFormStavkaProknjizenaHandler: {stavkaProknjizena.IdStavke}");
			var konto = await kontoRepo.GetAsync(stavkaProknjizena.IdKonto);
			StavkaForm nalog = new StavkaForm()
			{
				Id = stavkaProknjizena.IdStavke,
				IdKonto = stavkaProknjizena.IdKonto,
				IdNaloga = stavkaProknjizena.IdNaloga,
				DatumKnjizenja = stavkaProknjizena.DatumKnjizenja,
				Konto = konto.Sifra,
				Opis = stavkaProknjizena.Opis,
				Duguje = stavkaProknjizena.Duguje,
				Potrazuje = stavkaProknjizena.Potrazuje
			};
			stavkaRepo.Add(nalog);
		}
	}
}
