﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class NalogFormNalogOtvorenHandler : INotificationHandler<NalogOtvoren>
	{
		private readonly ILogger<NalogFormNalogOtvorenHandler> logger;
		private readonly INalogFormRepository nalogRepo;

		public NalogFormNalogOtvorenHandler(INalogFormRepository nalogRepo,
			ILogger<NalogFormNalogOtvorenHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
		}

		public Task Handle(NalogOtvoren nalogOtvoren, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling NalogFormNalogOtvorenHandler: {nalogOtvoren.IdNaloga}");
			NalogForm nalog = new NalogForm()
			{
				Id = nalogOtvoren.IdNaloga,
				IdTip = nalogOtvoren.IdTip,
				Datum = nalogOtvoren.DatumNaloga,
				Opis = nalogOtvoren.Opis,
			};
			nalogRepo.Add(nalog);
			return Task.CompletedTask;
		}
	}
}
