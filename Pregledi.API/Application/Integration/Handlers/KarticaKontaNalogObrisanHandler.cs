﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class KarticaKontaNalogObrisanHandler : INotificationHandler<NalogObrisan>
	{
		private readonly ILogger<KarticaKontaNalogObrisanHandler> logger;
		private readonly IKarticaKontaRepository karticaRepo;

		public KarticaKontaNalogObrisanHandler(IKarticaKontaRepository karticaRepo,
			ILogger<KarticaKontaNalogObrisanHandler> logger)
		{
			this.karticaRepo = karticaRepo;
			this.logger = logger;
		}

		public async Task Handle(NalogObrisan nalogObrisan, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling KarticaKontaNalogObrisanHandler: {nalogObrisan.IdNaloga}");
			await karticaRepo.DeleteNalogAsync(nalogObrisan.IdNaloga);
		}
	}
}
