﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class KarticaKontaIzmenjenoZaglavljeNalogaHandler : INotificationHandler<IzmenjenoZaglavljeNaloga>
	{
		private readonly ILogger<NalogFormIzmenjenoZaglavljeNalogaHandler> logger;
		private readonly IKarticaKontaRepository karticaRepo;

		public KarticaKontaIzmenjenoZaglavljeNalogaHandler(IKarticaKontaRepository karticaRepo, 
			ILogger<NalogFormIzmenjenoZaglavljeNalogaHandler> logger)
		{
			this.karticaRepo = karticaRepo;
			this.logger = logger;
		}

		public async Task Handle(IzmenjenoZaglavljeNaloga izmenjenoZaglavlje, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling KarticaKontaIzmenjenoZaglavljeNalogaHandler: {izmenjenoZaglavlje.IdNaloga}");
			await karticaRepo.UpdateDatumNalogaAsync(izmenjenoZaglavlje.IdNaloga, izmenjenoZaglavlje.DatumNaloga);
			await karticaRepo.UpdateTipNalogaAsync(izmenjenoZaglavlje.IdNaloga, izmenjenoZaglavlje.IdTip);
		}
	}
}
