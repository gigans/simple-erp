﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.EventStore;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Integration.Notifications;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Hadlers
{
	public class GlavnaKnjigaStavkaUklonjenaHandler : INotificationHandler<StavkaUklonjena>
	{
		private readonly ILogger<GlavnaKnjigaStavkaUklonjenaHandler> logger;
		private readonly INalogGKRepository nalogRepo;
		private readonly INotificationQueue notifications;

		public GlavnaKnjigaStavkaUklonjenaHandler(INalogGKRepository nalogRepo,
			INotificationQueue notifications,
			ILogger<GlavnaKnjigaStavkaUklonjenaHandler> logger)
		{
			this.nalogRepo = nalogRepo;
			this.logger = logger;
			this.notifications = notifications;
		}

		public async Task Handle(StavkaUklonjena stavkaUklonjena, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling GlavnaKnjigaStavkaUklonjenaHandler: {stavkaUklonjena.IdNaloga}");
			var nalog = await nalogRepo.GetAsync(stavkaUklonjena.IdNaloga);
			if (stavkaUklonjena.EventNumber > nalog.Version)
			{
				nalog.Version = stavkaUklonjena.EventNumber;
				nalog.BrojStavki--;
				nalog.UkupnoDuguje -= stavkaUklonjena.Duguje;
				nalog.UkupnoPotrazuje -= stavkaUklonjena.Potrazuje;
				nalog.UkupnoSaldo = nalog.UkupnoDuguje - nalog.UkupnoPotrazuje;
				notifications.Add(new GlavnaKnjigaChanged(stavkaUklonjena.EventId));
			}
		}
	}
}
