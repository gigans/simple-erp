﻿using MediatR;
using Microsoft.Extensions.Logging;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Handlers
{
    public class KarticaKontaStavkaUklonjenaHandler : INotificationHandler<StavkaUklonjena>
	{
		private readonly ILogger<StavkaFormStavkaUklonjenaHandler> logger;
		private readonly IKarticaKontaRepository karticaRepo;

		public KarticaKontaStavkaUklonjenaHandler(IKarticaKontaRepository karticaRepo,
			ILogger<StavkaFormStavkaUklonjenaHandler> logger)
		{
			this.karticaRepo = karticaRepo;
			this.logger = logger;
		}

		public async Task Handle(StavkaUklonjena stavkaUklonjena, CancellationToken cancellationToken)
		{
			logger.LogDebug($"Handling KarticaKontaStavkaUklonjenaHandler: {stavkaUklonjena.IdStavke}");
			var stavkaKartice = await karticaRepo.GetAsync(stavkaUklonjena.IdStavke);
			await karticaRepo.RemoveAsync(stavkaKartice);
		}
	}
}
