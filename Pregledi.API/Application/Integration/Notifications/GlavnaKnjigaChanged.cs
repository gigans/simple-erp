﻿using Integration.Contracts.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Notifications
{
	public class GlavnaKnjigaChanged : IGlavnaKnjigaChanged
	{
		public Guid EventId { get; }

		public GlavnaKnjigaChanged(Guid eventId)
		{
			this.EventId = eventId;
		}
	}
}
