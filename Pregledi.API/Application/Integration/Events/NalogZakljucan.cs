﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Integration.Events
{
	public class NalogZakljucan : EventBase
	{
		public Guid IdNaloga { get; }

		public NalogZakljucan(Guid idNaloga)
		{
			this.IdNaloga = idNaloga;
		}
	}
}
