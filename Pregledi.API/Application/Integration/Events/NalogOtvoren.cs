﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Integration.Events
{
    public class NalogOtvoren : EventBase
	{
		public Guid IdNaloga { get; }
		public DateTime DatumNaloga { get; }
		public long IdTip { get; set; }
		public string Opis { get; set; }

		public NalogOtvoren(Guid idNaloga, DateTime datumNaloga, long idTip, string opis)
		{
			this.IdNaloga = idNaloga;
			this.DatumNaloga = datumNaloga;
			this.IdTip = idTip;
			this.Opis = opis;
		}
	}
}
