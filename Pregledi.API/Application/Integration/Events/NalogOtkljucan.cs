﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Integration.Events
{
	public class NalogOtkljucan : EventBase
	{
		public Guid IdNaloga { get; }

		public NalogOtkljucan(Guid idNaloga)
		{
			this.IdNaloga = idNaloga;
		}
	}
}
