﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Integration.Events
{
	public class NalogObrisan : EventBase
	{
		public Guid IdNaloga { get; }

		public NalogObrisan(Guid idNaloga)
		{
			this.IdNaloga = idNaloga;
		}
	}
}
