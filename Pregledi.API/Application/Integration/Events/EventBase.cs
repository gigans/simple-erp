﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Integration.Events
{
	public class EventBase : INotification
    {
		public Guid EventId { get; set; }
		public long EventNumber { get; set; }
		public DateTime Created { get; set; }
    }
}
