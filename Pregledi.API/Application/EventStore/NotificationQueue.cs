﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.EventStore
{
	public interface INotificationQueue
	{
		void Add<T>(T notification);
		Dictionary<Type, object> GetAll();
	}

	public class NotificationQueue : INotificationQueue
	{
		private readonly ConcurrentDictionary<Type, object> notifications;

		public NotificationQueue()
		{
			this.notifications = new ConcurrentDictionary<Type, object>();
		}

		public void Add<T>(T notification)
		{
			notifications.AddOrUpdate(typeof(T), notification, (k, v) => notification);
		}

		public Dictionary<Type, object> GetAll()
		{
			return notifications.ToDictionary(k => k.Key, v => v.Value);
		}
	}
}
