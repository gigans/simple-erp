﻿using EventStore.ClientAPI;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Pregledi.API.Application.Data.Repositories;
using Pregledi.API.Application.Integration.Events;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pregledi.API.Application.EventStore
{
	public interface IEventResolver
	{
		Task ResolveAsync(ResolvedEvent @event);
	}

	public class EventResolver : IEventResolver
	{
		private readonly ILogger<EventResolver> logger;
		private readonly IMediator mediator;
		private readonly IList<Type> eventTypes;
		private readonly IProcessedEventRepository processedEventRepo;
		private readonly INotificationQueue notifications;
		private readonly IBus eventBus;

		public EventResolver(IMediator mediator, ILogger<EventResolver> logger,
			IProcessedEventRepository processedEventRepo, INotificationQueue notifications, IBus eventBus)
		{
			this.mediator = mediator;
			this.logger = logger;
			this.eventTypes = typeof(EventBase).Assembly.GetTypes()
				.Where(x => x.IsSubclassOf(typeof(EventBase))).ToList();
			this.processedEventRepo = processedEventRepo;
			this.notifications = notifications;
			this.eventBus = eventBus;
		}

		public async Task ResolveAsync(ResolvedEvent @event)
		{
			var eventType = eventTypes.Where(x => x.Name == @event.Event.EventType).SingleOrDefault();
			if (eventType != null)
			{
				string dataJson = Encoding.UTF8.GetString(@event.Event.Data);
				logger.LogDebug(JToken.Parse(dataJson).ToString(Formatting.Indented));
				var resolvedEvent = (EventBase)JsonConvert.DeserializeObject(dataJson, eventType);
				resolvedEvent.EventNumber = @event.Event.EventNumber;
				resolvedEvent.EventId = @event.Event.EventId;
				resolvedEvent.Created = @event.Event.Created;
				try
				{
					await mediator.Publish(resolvedEvent);
					await PersistEvent(@event);
					await PublishNotifications();
				}
				catch (Exception exc)
				{
					logger.LogError(exc,
						$@"Error handling event of type {@event.Event.EventType} id {@event.Event.EventId}
							{exc.InnerException?.InnerException?.Message ?? exc.InnerException?.Message ?? exc.Message}
							{exc.StackTrace}");
				}
			}
		}

		private async Task PersistEvent(ResolvedEvent @event)
		{
			var processed = new ProcessedEvent(@event.Event.EventId, 
						@event.OriginalStreamId,
						@event.Event.EventStreamId,
						@event.OriginalEventNumber,
						@event.OriginalPosition?.CommitPosition,
						@event.OriginalPosition?.PreparePosition,
						@event.Event.Created);
			processedEventRepo.Add(processed);
			await processedEventRepo.SaveAsync();
		}

		private async Task PublishNotifications()
		{
			foreach (var item in notifications.GetAll())
			{
				await eventBus.Publish(item.Value);
			}
		}
	}
}
