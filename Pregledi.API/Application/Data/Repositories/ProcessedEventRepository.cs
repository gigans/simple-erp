﻿using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.Repositories
{
	public interface IProcessedEventRepository
	{
		void Add(ProcessedEvent @event);
		Task SaveAsync();
		ProcessedEvent GetLastProcessed(string originalStream);
	}

	public class ProcessedEventRepository : IProcessedEventRepository
	{
		private readonly PreglediContext context;

		public ProcessedEventRepository(PreglediContext context)
		{
			this.context = context;
		}

		public void Add(ProcessedEvent @event)
		{
			context.ProcessedEvents.Add(@event);
		}

		public async Task SaveAsync()
		{
			await context.SaveChangesAsync();
		}

		public ProcessedEvent GetLastProcessed(string originalStream)
		{
			return context.ProcessedEvents
				.Where(x => x.OriginalStream == originalStream)
				.OrderByDescending(x => x.Checkpoint)
				.FirstOrDefault();
		}
	}
}
