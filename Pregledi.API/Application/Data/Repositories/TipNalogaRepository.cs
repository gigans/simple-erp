﻿using Microsoft.EntityFrameworkCore;
using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.Repositories
{
	public interface ITipNalogaRepository
	{
		Task<string> GetNazivAsync(long id);
	}

    public class TipNalogaRepository : ITipNalogaRepository
	{
		private readonly PreglediContext context;

		public TipNalogaRepository(PreglediContext context)
		{
			this.context = context;
		}

		public async Task<string> GetNazivAsync(long id)
		{
			return await context.TipoviNaloga.Where(x => x.Id == id).Select(x => x.Naziv).SingleOrDefaultAsync();
		}
    }
}
