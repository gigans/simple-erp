﻿using Microsoft.EntityFrameworkCore;
using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.Repositories
{
	public interface INalogFormRepository
	{
		void Add(NalogForm nalog);
		Task<NalogForm> GetAsync(Guid id);
		Task<NalogForm> GetSaStavkamaAsync(Guid id);
		void Remove(NalogForm nalog);
	}

	public class NalogFormRepository : INalogFormRepository
	{
		private readonly PreglediContext context;

		public NalogFormRepository(PreglediContext context)
		{
			this.context = context;
		}

		public async Task<NalogForm> GetAsync(Guid id)
		{
			return await context.NaloziForm.FindAsync(id);
		}

		public void Add(NalogForm nalog)
		{
			context.NaloziForm.Add(nalog);
		}

		public async Task<NalogForm> GetSaStavkamaAsync(Guid id)
		{
			return await context.NaloziForm.Where(x => x.Id == id)
				.Include(x => x.Stavke).SingleOrDefaultAsync();
		}

		public void Remove(NalogForm nalog)
		{
			context.NaloziForm.Remove(nalog);
		}
	}
}
