﻿using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.Repositories
{
	public interface IStavkaFormRepository
	{
		void Add(StavkaForm stavka);
		Task<StavkaForm> GetAsync(Guid id);
		void Remove(StavkaForm stavka);
	}

	public class StavkaFormRepository : IStavkaFormRepository
	{
		private readonly PreglediContext context;

		public StavkaFormRepository(PreglediContext context)
		{
			this.context = context;
		}

		public async Task<StavkaForm> GetAsync(Guid id)
		{
			return await context.StavkeForm.FindAsync(id);
		}

		public void Add(StavkaForm stavka)
		{
			context.StavkeForm.Add(stavka);
		}

		public void Remove(StavkaForm stavka)
		{
			context.StavkeForm.Remove(stavka);
		}
	}
}
