﻿using Microsoft.EntityFrameworkCore;
using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.Repositories
{
	public interface IKontoRepository
	{
		Task<Konto> GetAsync(long id);
	}

    public class KontoRepository : IKontoRepository
	{
		private readonly PreglediContext context;

		public KontoRepository(PreglediContext context)
		{
			this.context = context;
		}

		public async Task<Konto> GetAsync(long id)
		{
			return await context.Konta.FindAsync(id);
		}
    }
}
