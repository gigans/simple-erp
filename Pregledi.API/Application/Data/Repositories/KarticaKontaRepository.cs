﻿using Microsoft.EntityFrameworkCore;
using Pregledi.API.Application.Data.EntityFramework;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.Repositories
{
	public interface IKarticaKontaRepository
	{
		Task AddAsync(KarticaKonta stavka);
		Task<KarticaKonta> GetAsync(Guid id);
		Task RemoveAsync(KarticaKonta stavka);
		Task<IList<KarticaKonta>> GetAllAsync(long idKonto);
		Task UpdateDatumNalogaAsync(Guid idNaloga, DateTime noviDatum);
		Task UpdateTipNalogaAsync(Guid idNaloga, long idTipNovi);
		Task DeleteNalogAsync(Guid idNaloga);
	}

	public class KarticaKontaRepository : IKarticaKontaRepository
	{
		private readonly PreglediContext context;

		public KarticaKontaRepository(PreglediContext context)
		{
			this.context = context;
		}

		public async Task<KarticaKonta> GetAsync(Guid id)
		{
			return await context.KarticaKonta.FindAsync(id);
		}

		public async Task AddAsync(KarticaKonta stavka)
		{
			var prethodnaStavka = await GetPrethodnaStavka(stavka.IdKonto, stavka.DatumNaloga,
				stavka.DatumKnjizenja, stavka.Created);
			if (prethodnaStavka != null)
			{
				stavka.SaldoKumulativno = stavka.Saldo + prethodnaStavka.SaldoKumulativno;
			}
			else
			{
				stavka.SaldoKumulativno = stavka.Saldo;
			}
			var naredneStavke = await GetNaredneStavke(stavka.IdKonto,
				stavka.DatumNaloga, stavka.DatumKnjizenja, stavka.Created);
			foreach (var item in naredneStavke)
			{
				item.SaldoKumulativno += stavka.Saldo;
			}
			context.KarticaKonta.Add(stavka);
		}

		public async Task RemoveAsync(KarticaKonta stavka)
		{
			var naredneStavke = await GetNaredneStavke(stavka.IdKonto,
				stavka.DatumNaloga, stavka.DatumKnjizenja, stavka.Created);
			foreach (var item in naredneStavke)
			{
				item.SaldoKumulativno -= stavka.Saldo;
			}
			context.KarticaKonta.Remove(stavka);
		}

		public async Task UpdateTipNalogaAsync(Guid idNaloga, long idTipNovi)
		{
			string noviTipNaziv = await context.TipoviNaloga.Where(x => x.Id == idTipNovi)
				.Select(x => x.Naziv).SingleOrDefaultAsync();
			
			var stavkeNaloga = await context.KarticaKonta.Where(x => x.IdNaloga == idNaloga)
				.ToListAsync();
			foreach (var item in stavkeNaloga)
			{
				item.TipNaloga = noviTipNaziv;
			}
		}

		public async Task UpdateDatumNalogaAsync(Guid idNaloga, DateTime noviDatum)
		{
			bool promenjenDatum = await context.KarticaKonta
				.AnyAsync(x => x.IdNaloga == idNaloga && x.DatumNaloga != noviDatum);
			if (!promenjenDatum)
				return;

			var stavkeNaloga = await context.KarticaKonta.Where(x => x.IdNaloga == idNaloga)
				.OrderBy(x => x.DatumNaloga)
				.ThenBy(x => x.DatumKnjizenja)
				.ThenBy(x => x.Created)
				.ToListAsync();
			foreach (var sn in stavkeNaloga)
			{
				var naredneStavkeStariDatum = await GetNaredneStavke(sn.IdKonto, sn.DatumNaloga,
					sn.DatumKnjizenja, sn.Created);
				foreach (var item in naredneStavkeStariDatum)
				{
					item.SaldoKumulativno -= sn.Saldo;
				}

				sn.DatumNaloga = noviDatum;

				var prethodnaStavka = await GetPrethodnaStavka(sn.IdKonto, sn.DatumNaloga,
					sn.DatumKnjizenja, sn.Created);
				if (prethodnaStavka != null && prethodnaStavka.Id != sn.Id)
				{
					sn.SaldoKumulativno = sn.Saldo + prethodnaStavka.SaldoKumulativno;
				}
				else
				{
					sn.SaldoKumulativno = sn.Saldo;
				}
				var naredneStavkeNoviDatum = await GetNaredneStavke(sn.IdKonto,
					sn.DatumNaloga, sn.DatumKnjizenja, sn.Created);
				foreach (var item in naredneStavkeNoviDatum)
				{
					if (item.Id != sn.Id)
						item.SaldoKumulativno += sn.Saldo;
				}

				int stavkaIdx = stavkeNaloga.IndexOf(sn);
				sn.Created = context.Entry(stavkeNaloga[stavkeNaloga.Count - stavkaIdx - 1])
					.Property(x => x.Created).OriginalValue;
			}
		}

		public async Task DeleteNalogAsync(Guid idNaloga)
		{
			var stavkeNaloga = await context.KarticaKonta.Where(x => x.IdNaloga == idNaloga)
				.OrderBy(x => x.DatumNaloga)
				.ThenBy(x => x.DatumKnjizenja)
				.ThenBy(x => x.Created)
				.ToListAsync();
			foreach (var sn in stavkeNaloga)
			{
				var naredneStavke = await GetNaredneStavke(sn.IdKonto, sn.DatumNaloga,
					sn.DatumKnjizenja, sn.Created);
				foreach (var item in naredneStavke)
				{
					item.SaldoKumulativno -= sn.Saldo;
				}
				context.KarticaKonta.Remove(sn);
			}
		}

		public async Task<IList<KarticaKonta>> GetAllAsync(long idKonto)
		{
			return await context.KarticaKonta.Where(x => x.IdKonto == idKonto)
				.OrderBy(x => x.DatumNaloga)
				.ThenBy(x => x.DatumKnjizenja)
				.ThenBy(x => x.Created)
				.AsNoTracking()
				.ToListAsync();
		}

		private async Task<IList<KarticaKonta>> GetNaredneStavke(long idKonto, DateTime datumNaloga,
			DateTime datumKnjizenja, DateTime created)
		{
			var naredneStavke = await context.KarticaKonta
				.Where(x => x.IdKonto == idKonto &&
					(x.DatumNaloga > datumNaloga ||
						(x.DatumNaloga == datumNaloga &&
							x.DatumKnjizenja > datumKnjizenja) ||
								(x.DatumNaloga == datumNaloga &&
								x.DatumKnjizenja == datumKnjizenja &&
									x.Created > created)))
				.ToListAsync();
			return naredneStavke;
		}

		private async Task<KarticaKonta> GetPrethodnaStavka(long idKonto, DateTime datumNaloga,
			DateTime datumKnjizenja, DateTime created)
		{
			var prethodnaStavka = await context.KarticaKonta.Where(x => x.IdKonto == idKonto)
				.Where(x => x.IdKonto == idKonto &&
					(x.DatumNaloga < datumNaloga ||
						(x.DatumNaloga == datumNaloga &&
							x.DatumKnjizenja < datumKnjizenja) ||
								(x.DatumNaloga == datumNaloga &&
								x.DatumKnjizenja == datumKnjizenja &&
									x.Created < created)))
				.OrderByDescending(x => x.DatumNaloga)
				.ThenByDescending(x => x.DatumKnjizenja)
				.ThenByDescending(x => x.Created)
				.FirstOrDefaultAsync();
			return prethodnaStavka;
		}
	}
}
