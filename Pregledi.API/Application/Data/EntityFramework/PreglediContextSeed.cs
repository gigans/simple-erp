﻿
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.EntityFramework
{
	public class PreglediContextSeed
	{
		public void Seed(PreglediContext context)
		{
			CreateLogTableIfNotExists(context);

			//if(!context.Database.IsInMemory())
			//	context.Database.Migrate();

			if (!context.TipoviNaloga.Any())
			{
				context.TipoviNaloga.AddRange(new[]
				{
					new TipNaloga() { Id = 1, Naziv="Ulazne fakture" },
					new TipNaloga() { Id = 2, Naziv="Izvodi" },
				});
			}
			if (!context.Konta.Any())
			{
				context.Konta.AddRange(new[]
				{
					new Konto() { Sifra = "435", Naziv = "zaduzenje" },
					new Konto() { Sifra = "241", Naziv = "tekuci racun" },
				});
			}

			context.SaveChanges();
		}

		private void CreateLogTableIfNotExists(PreglediContext context)
		{
			string command = $@"CREATE TABLE IF NOT EXISTS log
							( 
							ID serial primary key,
							entered_date date default Now(),
							log_application varchar(200) NULL, 
							log_date character varying(100) NULL, 
							log_level character varying(100) NULL, 
							log_logger character varying(8000) NULL, 
							log_message character varying(8000) NULL, 
							log_machine_name character varying(8000) NULL, 
							log_user_name character varying(8000) NULL, 
							log_call_site character varying(8000) NULL, 
							log_thread character varying(100) NULL, 
							log_exception character varying(8000) NULL, 
							log_stacktrace character varying(8000) NULL 
							)";
			context.Database.ExecuteSqlCommand(command);
		}
	}
}
