﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Data.EntityFramework.Configurations
{
	class ProcessedEventConfiguration : IEntityTypeConfiguration<ProcessedEvent>
	{
		public void Configure(EntityTypeBuilder<ProcessedEvent> builder)
		{
			builder.ToTable("processed_event");
			builder.Property(x => x.Stream).HasMaxLength(255).IsRequired();
		}
	}
}
