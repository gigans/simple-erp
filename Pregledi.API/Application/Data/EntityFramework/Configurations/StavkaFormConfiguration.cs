﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Data.EntityFramework.Configurations
{
	class StavkaFormConfiguration : IEntityTypeConfiguration<StavkaForm>
	{
		public void Configure(EntityTypeBuilder<StavkaForm> builder)
		{
			builder.ToTable("stavka_form");
			builder.Property(x => x.DatumKnjizenja).HasColumnType("date");
			builder.Property(x => x.Konto).HasMaxLength(10).IsRequired();
			builder.Property(x => x.Version).IsConcurrencyToken();
		}
	}
}
