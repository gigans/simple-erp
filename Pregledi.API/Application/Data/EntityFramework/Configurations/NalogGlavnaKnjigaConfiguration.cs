﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Data.EntityFramework.Configurations
{
	class NalogGlavnaKnjigaConfiguration : IEntityTypeConfiguration<NalogGlavnaKnjiga>
	{
		public void Configure(EntityTypeBuilder<NalogGlavnaKnjiga> builder)
		{
			builder.ToTable("nalog_glavna_knjiga");
			builder.Property(x => x.Datum).HasColumnType("date");
			builder.Property(x => x.Version).IsConcurrencyToken();
		}
	}
}
