﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Data.EntityFramework.Configurations
{
	class KontoConfiguration : IEntityTypeConfiguration<Konto>
	{
		public void Configure(EntityTypeBuilder<Konto> builder)
		{
			builder.ToTable("konto");
		}
	}
}
