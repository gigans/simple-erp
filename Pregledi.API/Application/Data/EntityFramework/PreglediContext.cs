﻿using Microsoft.EntityFrameworkCore;
using Pregledi.API.Application.Data.EntityFramework.Configurations;
using Pregledi.API.Application.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Data.EntityFramework
{
    public class PreglediContext : DbContext
	{
		public Guid Guid = Guid.NewGuid();
		public DbSet<NalogGlavnaKnjiga> NaloziGlavneKnjige { get; set; }
		public DbSet<NalogForm> NaloziForm { get; set; }
		public DbSet<StavkaForm> StavkeForm { get; set; }
		public DbSet<KarticaKonta> KarticaKonta { get; set; }
		public DbSet<Konto> Konta { get; set; }
		public DbSet<TipNaloga> TipoviNaloga { get; set; }
		public DbSet<ProcessedEvent> ProcessedEvents { get; set; }

		public PreglediContext()
		{
			
		}

		public PreglediContext(DbContextOptions options) : base(options)
		{
			//Database.EnsureDeleted();
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new KontoConfiguration());
			modelBuilder.ApplyConfiguration(new TipNalogaConfiguration());
			modelBuilder.ApplyConfiguration(new NalogGlavnaKnjigaConfiguration());
			modelBuilder.ApplyConfiguration(new ProcessedEventConfiguration());
			modelBuilder.ApplyConfiguration(new NalogFormConfiguration());
			modelBuilder.ApplyConfiguration(new StavkaFormConfiguration());
			modelBuilder.ApplyConfiguration(new KarticaKontaConfiguration());
		}

		public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await SaveChangesAsync();
			return true;
		}
	}
}
