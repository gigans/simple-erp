﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pregledi.API.Application.Model
{
    public class TipNaloga
    {
		public long Id { get; set; }
		public string Naziv { get; set; }
	}
}
