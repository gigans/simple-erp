﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Model
{
    public class NalogForm
    {
		public Guid Id { get; set; }
		public DateTime Datum { get; set; }
		public long IdTip { get; set; }
		public string Opis { get; set; }
		public long Version { get; set; }

		public IList<StavkaForm> Stavke { get; set; }
	}
}
