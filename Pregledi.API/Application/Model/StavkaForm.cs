﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Model
{
    public class StavkaForm
    {
		public Guid Id { get; set; }
		public Guid IdNaloga { get; set; }
		public long IdKonto { get; set; }
		public string Konto { get; set; }
		public DateTime DatumKnjizenja { get; set; }
		public decimal Duguje { get; set; }
		public decimal Potrazuje { get; set; }
		public string Opis { get; set; }
		public long Version { get; set; }
	}
}
