﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Model
{
    public class NalogGlavnaKnjiga
    {
		public Guid Id { get; set; }
		public DateTime Datum { get; set; }
		public string TipNaziv { get; set; }
		public string Opis { get; set; }
		public int BrojStavki { get; set; }
		public decimal UkupnoDuguje { get; set; }
		public decimal UkupnoPotrazuje { get; set; }
		public decimal UkupnoSaldo { get; set; }
		public bool Zakljucan { get; set; }
		public long Version { get; set; }
    }
}
