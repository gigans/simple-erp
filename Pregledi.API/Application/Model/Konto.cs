﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pregledi.API.Application.Model
{
    public class Konto
    {
		public long Id { get; set; }
		public string Sifra { get; set; }
		public string Naziv { get; set; }
    }
}
