﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Contracts.Events
{
    public interface IValidationError
    {
		string Property { get; }
		string Message { get; }
	}
}
