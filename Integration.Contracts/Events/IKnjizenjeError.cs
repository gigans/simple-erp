﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Integration.Contracts.Events
{
    public interface IKnjizenjeError
    {
		string Message { get; }
		IEnumerable<IValidationError> ValidationErrors { get; }
	}
}
