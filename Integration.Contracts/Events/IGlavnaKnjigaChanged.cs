﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Events
{
    public interface IGlavnaKnjigaChanged
    {
		Guid EventId { get; }
	}
}
