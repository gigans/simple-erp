﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
    public interface IStavkaIzvoda
    {
		int SifraPlacanja { get; set; }
		decimal Duguje { get; set; }
		decimal Potrazuje { get; set; }
	}
}
