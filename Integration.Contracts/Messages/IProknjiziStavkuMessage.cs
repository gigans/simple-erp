﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
	public interface IProknjiziStavkuMessage
	{
		Guid CommandId { get; }
		Guid IdNaloga { get; }
		long IdKonto { get; }
		decimal Duguje { get; }
		decimal Potrazuje { get; }
		string Opis { get; }
	}
}
