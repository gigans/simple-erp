﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
    public interface IStornirajStavkuMessage
	{
		Guid CommandId { get; }
		Guid IdNaloga { get; }
		Guid IdStavke { get; }
    }
}
