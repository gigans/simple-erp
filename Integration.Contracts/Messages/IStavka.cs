﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
    public interface IStavka
    {
		long IdKonto { get; set; }
		decimal Duguje { get; set; }
		decimal Potrazuje { get; set; }
		string Opis { get; set; }
	}
}
