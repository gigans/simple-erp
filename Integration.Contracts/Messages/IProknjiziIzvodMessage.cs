﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
	public interface IProknjiziIzvodMessage
	{
		Guid CommandId { get; set; }
		DateTime Datum { get; set; }
		ICollection<IStavkaIzvoda> Stavke { get; set; }
	}
}
