﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
    public interface IOtkljucajNalogMessage
	{
		Guid CommandId { get; }
		Guid IdNaloga { get; }
    }
}
