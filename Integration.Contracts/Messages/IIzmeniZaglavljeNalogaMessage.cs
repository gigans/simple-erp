﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
    public interface IIzmeniZaglavljeNalogaMessage
	{
		Guid CommandId { get; }
		Guid IdNaloga { get; }
		long IdTip { get; }
		DateTime DatumNaloga { get; }
		string Opis { get; }
    }
}
