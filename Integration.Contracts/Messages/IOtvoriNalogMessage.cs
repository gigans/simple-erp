﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Integration.Contracts.Messages
{
    public interface IOtvoriNalogMessage
    {
		Guid CommandId { get; }
		long IdTip { get; }
		DateTime DatumNaloga { get; }
		string Opis { get; }
		List<IStavka> Stavke { get; }
	}
}
