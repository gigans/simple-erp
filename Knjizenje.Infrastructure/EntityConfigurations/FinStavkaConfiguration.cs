﻿using Knjizenje.Domain.Entities.FinNalogAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Infrastructure.EntityConfigurations
{
	class FinStavkaConfiguration : IEntityTypeConfiguration<FinStavka>
	{
		public void Configure(EntityTypeBuilder<FinStavka> builder)
		{
			builder.ToTable("fin_stavka");
			builder.OwnsOne(x => x.Iznos, b =>
			{
				b.Property(x => x.Duguje).HasColumnName("Duguje");
				b.Property(x => x.Potrazuje).HasColumnName("Potrazuje");
			});
			builder.HasOne(x => x.Nalog).WithMany(x => x.Stavke);
			builder.HasOne(x => x.Konto).WithMany().HasForeignKey(x => x.IdKonto);
		}
	}
}
