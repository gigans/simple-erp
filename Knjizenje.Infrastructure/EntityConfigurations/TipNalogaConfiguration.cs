﻿using Knjizenje.Domain.Entities;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Infrastructure.EntityConfigurations
{
	class TipNalogaConfiguration : IEntityTypeConfiguration<TipNaloga>
	{
		public void Configure(EntityTypeBuilder<TipNaloga> builder)
		{
			builder.ToTable("tip_naloga");
		}
	}
}
