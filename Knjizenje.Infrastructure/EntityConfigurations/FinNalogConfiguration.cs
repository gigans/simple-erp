﻿using Knjizenje.Domain.Entities.FinNalogAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Infrastructure.EntityConfigurations
{
	class FinNalogConfiguration : IEntityTypeConfiguration<FinNalog>
	{
		public void Configure(EntityTypeBuilder<FinNalog> builder)
		{
			builder.ToTable("fin_nalog");
			builder.OwnsOne(x => x.Bilans, b =>
			{
				b.Property(x => x.Duguje).HasColumnName("Duguje");
				b.Property(x => x.Potrazuje).HasColumnName("Potrazuje");
			});
			builder.HasOne(x => x.Tip).WithMany().HasForeignKey(x => x.IdTip);
			builder.HasIndex(x => new { x.IdTip, x.DatumNaloga }).IsUnique();
			builder.HasIndex(x => new { x.IdTip, x.Godina, x.RedniBroj }).IsUnique();
		}
	}
}
