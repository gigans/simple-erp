﻿using Knjizenje.Domain.Seedwork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Knjizenje.Infrastructure.Context
{
    public class QueryableContext
    {
		private readonly KnjizenjeContext context;

		public QueryableContext(KnjizenjeContext context)
		{
			this.context = context;
		}

		public IQueryable<T> Set<T>()
			where T : Entity
		{
			return context.Set<T>().AsQueryable().AsNoTracking();
		}
	}
}
