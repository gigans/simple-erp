﻿using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Seedwork;
using Knjizenje.Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.Infrastructure.Context
{
	public class KnjizenjeContext : DbContext, IUnitOfWork
	{
		public DbSet<TipNaloga> TipoviNaloga { get; set; }
		public DbSet<Konto> Konta { get; set; }

		public KnjizenjeContext()
		{

		}

		public KnjizenjeContext(DbContextOptions options) : base(options)
		{
			//Database.EnsureDeleted();
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.ApplyConfiguration(new KontoConfiguration());
			modelBuilder.ApplyConfiguration(new TipNalogaConfiguration());
		}

		public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await SaveChangesAsync();
			return true;
		}
	}
}
