﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Infrastructure.EventSourcing
{
    public class EventMetadata
    {
		public Guid CommandId { get; set; }

		public EventMetadata(Guid commandId)
		{
			this.CommandId = commandId;
		}
    }
}
