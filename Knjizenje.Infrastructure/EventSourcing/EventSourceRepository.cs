﻿using Knjizenje.Domain.Seedwork;
using Knjizenje.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Infrastructure.EventSourcing
{
	public class EventSourceRepository<TAggregate, TKey> : IAggregateRepository<TAggregate, TKey>
		where TKey : IAggregateId
	{
		private readonly IEventStore eventStore;

		public EventSourceRepository(IEventStore eventStore)
		{
			this.eventStore = eventStore;
		}

		public async Task<TAggregate> GetAsync(TKey id)
		{
			var events = await eventStore.ReadEventsAsync(id.IdAsString());
			var aggregate = Reconstruct(events);
			return aggregate;
		}

		public async Task SaveAsync(AggregateRoot<TAggregate, TKey> aggregate, Guid commandId)
		{
			await eventStore.WriteEventsAsync(aggregate.Id.IdAsString(), aggregate.UncommittedEvents, aggregate.Version, commandId);
			aggregate.ClearUncommittedEvents();
		}

		protected TAggregate Reconstruct(IList<EventBase> events)
		{
			IAggregateRoot aggregate = AggregateRoot<TAggregate, TKey>.CreateEmpty();
			foreach (var item in events)
			{
				aggregate.ApplyEvent(item);
			}

			return (TAggregate)aggregate;
		}
	}
}
