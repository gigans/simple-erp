﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.Projections;
using Knjizenje.Domain.Seedwork;
using Knjizenje.Domain.SeedWork;
using Knjizenje.Infrastructure.EventSourcing.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Infrastructure.EventSourcing
{
	public interface IEventStore
	{
		Task<IList<EventBase>> ReadEventsAsync(string stream);
		Task WriteEventsAsync(string stream, IEnumerable<EventBase> events, long expectedVersion, Guid commandId);
		Task<IList<T>> GetProjectionAsync<T>(string projection);
	}

	public class EventStore : IEventStore
	{
		private readonly IEventStoreConnection connection;
		private readonly ProjectionsManager projectionManager;

		public EventStore(IEventStoreConnection connection, ProjectionsManager projectionManager)
		{
			this.connection = connection;
			this.projectionManager = projectionManager;
		}

		public async Task<IList<T>> GetProjectionAsync<T>(string projection)
		{
			var state = await projectionManager.GetStateAsync(projection);
			if (string.IsNullOrEmpty(state))
				return Enumerable.Empty<T>().ToList();
			var eventSerializer = new EventSerializerContractResolver();
			var jsonSettings = new JsonSerializerSettings()
			{
				ContractResolver = eventSerializer
			};
			var stateObjects = JsonConvert.DeserializeObject<List<T>>(state, jsonSettings);
			return stateObjects;
		}

		public async Task<IList<EventBase>> ReadEventsAsync(string stream)
		{
			var ret = new List<EventBase>();
			StreamEventsSlice currentSlice;
			long nextSliceStart = StreamPosition.Start;
			var eventSerializer = new EventSerializerContractResolver();
			var jsonSettings = new JsonSerializerSettings()
			{
				ContractResolver = eventSerializer
			};
			var eventTypes = typeof(EventBase).Assembly.GetTypes().Where(x => x.IsSubclassOf(typeof(EventBase))).ToList();
			do
			{
				currentSlice = await connection.ReadStreamEventsForwardAsync(stream, nextSliceStart, 200, false);
				if (currentSlice.Status != SliceReadStatus.Success)
				{
					throw new Exception($"Stream '{stream}' not found");
				}
				nextSliceStart = currentSlice.NextEventNumber;
				foreach (var resolvedEvent in currentSlice.Events)
				{
					string json = Encoding.UTF8.GetString(resolvedEvent.Event.Data);
					Type eventType = eventTypes.Where(x => x.Name == resolvedEvent.Event.EventType).SingleOrDefault();
					//Type eventType = Type.GetType(resolvedEvent.Event.EventType);
					var @event = (EventBase)JsonConvert.DeserializeObject(json, eventType, jsonSettings);
					@event.EventNumber = resolvedEvent.Event.EventNumber;
					@event.EventId = resolvedEvent.Event.EventId;
					ret.Add(@event);
				}
			}
			while (!currentSlice.IsEndOfStream);
			return ret;
		}

		public async Task WriteEventsAsync(string stream, IEnumerable<EventBase> events, long expectedVersion, Guid commandId)
		{
			var eventSerializer = new EventSerializerContractResolver();
			var eventJsonSettings = new JsonSerializerSettings()
			{
				ContractResolver = eventSerializer
			};
			var eventMetadata = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new EventMetadata(commandId)));
			var eventData = events.Select(x =>
			{
				string json = JsonConvert.SerializeObject(x, eventJsonSettings);
				var serializedEvent = Encoding.UTF8.GetBytes(json);
				
				return new EventData(x.EventId, x.GetType().Name, true, serializedEvent, eventMetadata);
			});
			await connection.AppendToStreamAsync(stream, expectedVersion, eventData);
		}
	}
}
