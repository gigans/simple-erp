﻿using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Seedwork;
using Knjizenje.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Infrastructure.Repositories
{
	public class KontoRepository : IKontoRepository
	{
		private readonly KnjizenjeContext context;
		public IUnitOfWork UnitOfWork => context;

		public KontoRepository(KnjizenjeContext context)
		{
			this.context = context;
		}

		public async Task<Konto> GetBySifraAsync(string sifra)
		{
			return await context.Konta
				.Where(x => x.Sifra == sifra)
				.SingleOrDefaultAsync();
		}
	}
}
