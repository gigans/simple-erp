﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Seedwork;
using Knjizenje.Infrastructure.Context;
using Knjizenje.Infrastructure.EventSourcing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Infrastructure.Repositories
{
	public class FinNalogRepository : EventSourceRepository<FinNalog, FinNalogId>,
		IFinNalogRepository
	{
		private readonly IEventStore eventStore;

		public FinNalogRepository(IEventStore eventStore) : base(eventStore)
		{
			this.eventStore = eventStore;
		}

		public async Task<FinNalogId> GetPostojeciAsync(TipNaloga tip, DateTime datumNaloga)
		{
			var zaglavlja = await eventStore.GetProjectionAsync<ZaglavljeNaloga>("ZaglavljaNaloga");
			var nalog = zaglavlja.Where(x => x.DatumNaloga == datumNaloga && x.IdTip == tip.Id).SingleOrDefault();
			return nalog?.IdNaloga;
		}

		public async Task<ZaglavljeNaloga> GetZaglavljeAsync(FinNalogId id)
		{
			var zaglavlja = await eventStore.GetProjectionAsync<ZaglavljeNaloga>("ZaglavljaNaloga");
			var nalog = zaglavlja.Where(x => x.IdNaloga == id).SingleOrDefault();
			return nalog;
		}

		public async Task<ZaglavljeNaloga> GetZaglavljeAsync(TipNaloga tip, DateTime datumNaloga)
		{
			var zaglavlja = await eventStore.GetProjectionAsync<ZaglavljeNaloga>("ZaglavljaNaloga");
			var nalog = zaglavlja.Where(x => x.DatumNaloga == datumNaloga && x.IdTip == tip.Id).SingleOrDefault();
			return nalog;
		}
	}
}
