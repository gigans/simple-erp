﻿using Knjizenje.Domain.DTO;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Entities.KontoAggregate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Domain.Services
{
	public interface IIzvodService
	{
		Task<IList<FinStavka>> FormirajStavkeNaloga(IEnumerable<StavkaIzvoda> stavkeIzvoda);
	}

	public class IzvodService : IIzvodService
	{
		private readonly IKontoRepository kontoRepo;

		public IzvodService(IKontoRepository kontoRepo)
		{
			this.kontoRepo = kontoRepo;
		}

		public async Task<IList<FinStavka>> FormirajStavkeNaloga(IEnumerable<StavkaIzvoda> stavkeIzvoda)
		{
			if (stavkeIzvoda == null)
				throw new ArgumentNullException(nameof(stavkeIzvoda));
			if (!stavkeIzvoda.Any())
				return new List<FinStavka>();

			var konto435 = await kontoRepo.GetBySifraAsync("435");
			var konto241 = await kontoRepo.GetBySifraAsync("241");

			var stavke = stavkeIzvoda
					.Select(x =>
					{
						var stavka435 = FinStavka.Nova(konto435.Id, x.Duguje, x.Potrazuje, null);
						var stavka241 = FinStavka.Nova(konto241.Id, x.Potrazuje, x.Duguje, null);
						return new[] { stavka241, stavka435 };
					})
					.SelectMany(x => x)
					.ToList();

			return stavke;
		}
	}
}
