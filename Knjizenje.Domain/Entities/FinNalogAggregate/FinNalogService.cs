﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Domain.Entities.FinNalogAggregate
{
	public interface IFinNalogService
	{
		Task<FinNalog> OtvoriNalog(TipNaloga tip, DateTime datumNaloga, string opis,
			IEnumerable<FinStavka> stavke);
		Task IzmeniZaglavlje(FinNalog nalog, TipNaloga noviTip, DateTime noviDatumNaloga);
	}

	public class FinNalogService : IFinNalogService
    {
		private readonly IFinNalogRepository nalogRepo;

		public FinNalogService(IFinNalogRepository nalogRepo)
		{
			this.nalogRepo = nalogRepo;
		}

		public async Task<FinNalog> OtvoriNalog(TipNaloga tip, DateTime datumNaloga, string opis,
			IEnumerable<FinStavka> stavke)
		{
			int godina = datumNaloga.Year;
			int redniBroj = await nalogRepo.GetSledeciRedniBrojAsync(tip, godina);
			FinNalog nalog = new FinNalog(tip, redniBroj, datumNaloga, opis, stavke);
			return nalog;
		}

		public async Task IzmeniZaglavlje(FinNalog nalog, TipNaloga noviTip, DateTime noviDatumNaloga)
		{
			int godina = noviDatumNaloga.Year;
			int noviRedniBroj = await nalogRepo.GetSledeciRedniBrojAsync(noviTip, godina);
			nalog.IzmeniZaglavlje(noviTip, noviDatumNaloga, noviRedniBroj);
		}
	}
}
