﻿using Knjizenje.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.Entities.FinNalogAggregate
{
    public class TipNaloga : Enumeration
    {
		public static TipNaloga UlazneFakture = new TipNaloga(1, "Ulazne fakture");
		public static TipNaloga Izvodi = new TipNaloga(2, "Izvodi");

		public TipNaloga(long id, string name) : base(id, name) { }
	}
}
