﻿using Knjizenje.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.Entities.FinNalogAggregate
{
	public class FinNalogId : IAggregateId
	{
		private const string StreamPrefix = "FinNalog-";

		public Guid Id { get; private set; }

		private FinNalogId(Guid id)
		{
			Id = id;
		}

		public FinNalogId(string id)
		{
			Id = Guid.Parse(id.StartsWith(StreamPrefix) ?
				id.Substring(StreamPrefix.Length) : id);
		}

		public override string ToString()
		{
			return IdAsString();
		}

		public override bool Equals(object obj)
		{
			return obj is FinNalogId && Equals(Id, ((FinNalogId)obj).Id);
		}

		public override int GetHashCode()
		{
			return Id.GetHashCode();
		}

		public static FinNalogId Novi()
		{
			return new FinNalogId(Guid.NewGuid());
		}

		public string IdAsString()
		{
			return $"{StreamPrefix}{Id.ToString()}";
		}

		public static bool operator ==(FinNalogId left, FinNalogId right)
		{
			if (Object.Equals(left, null))
				return (Object.Equals(right, null)) ? true : false;
			else
				return left.Equals(right);
		}

		public static bool operator !=(FinNalogId left, FinNalogId right)
		{
			return !(left == right);
		}
	}
}
