﻿using Knjizenje.Domain.Events;
using Knjizenje.Domain.Exceptions;
using Knjizenje.Domain.Seedwork;
using Knjizenje.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Knjizenje.Domain.Entities.FinNalogAggregate
{
	public class FinNalog : AggregateRoot<FinNalog, FinNalogId>
	{
		public bool Zakljucan { get; private set; }
		public IReadOnlyCollection<FinStavka> Stavke => stavke.AsReadOnly();
		private readonly List<FinStavka> stavke;
		public bool Obrisan { get; private set; }

		internal FinNalog()
		{
			stavke = new List<FinStavka>();
			Obrisan = false;
		}

		public static FinNalog OtvoriNovi(TipNaloga tip, DateTime datumNaloga, string opis,
			IEnumerable<FinStavka> stavke)
		{
			if (tip == null)
				throw new ArgumentNullException(nameof(tip));
			if (stavke == null)
				throw new ArgumentNullException(nameof(stavke));
			if (!stavke.Any())
				throw new KnjizenjeException("Nalog mora imati bar jednu stavku");

			FinNalog noviNalog = new FinNalog();
			noviNalog.Id = FinNalogId.Novi();
			noviNalog.Obavesti(new NalogOtvoren(noviNalog.Id, datumNaloga, tip.Id, opis));

			foreach (var stavka in stavke)
			{
				noviNalog.ProknjiziStavku(stavka);
			}
			return noviNalog;
		}

		public void ProknjiziStavku(FinStavka stavka)
		{
			if (stavka == null)
				throw new ArgumentNullException(nameof(stavka));
			if (stavka.Id == Guid.Empty)
				throw new KnjizenjeException("Id stavke nije korektan");
			ProveriZakljucan();

			DateTime datumKnjizenja = DateTime.Today;
			Obavesti(new StavkaProknjizena(Id, stavka.Id, datumKnjizenja, stavka.IdKonto,
				stavka.Iznos.Duguje, stavka.Iznos.Potrazuje, stavka.Opis));
		}

		public void StornirajStavku(FinStavka originalStavka)
		{
			if (originalStavka == null)
				throw new ArgumentNullException(nameof(originalStavka));
			ProveriStavkaPripadaNalogu(originalStavka);

			var stornoStavka = FinStavka.Nova(originalStavka.IdKonto, -originalStavka.Iznos.Duguje,
				-originalStavka.Iznos.Potrazuje, null);
			ProknjiziStavku(stornoStavka);
		}

		public void UkloniStavku(FinStavka stavka)
		{
			if (stavka == null)
				throw new ArgumentNullException(nameof(stavka));
			ProveriZakljucan();
			if (!Obrisan && stavke.Count <= 1)
				throw new KnjizenjeException("Nalog mora imati bar jednu stavku");
			ProveriStavkaPripadaNalogu(stavka);

			Obavesti(new StavkaUklonjena(Id, stavka.Id, stavka.DatumKnjizenja, stavka.IdKonto,
				stavka.Iznos.Duguje, stavka.Iznos.Potrazuje, stavka.Opis));
		}

		public void Zakljucaj()
		{
			if (!Zakljucan)
				Obavesti(new NalogZakljucan(Id));
		}

		public void Otkljucaj()
		{
			if (Zakljucan)
				Obavesti(new NalogOtkljucan(Id));
		}

		public void IzmeniZaglavlje(TipNaloga tip, DateTime datumNaloga, string opis)
		{
			if (tip == null)
				throw new ArgumentNullException(nameof(tip));
			ProveriZakljucan();

			Obavesti(new IzmenjenoZaglavljeNaloga(Id, datumNaloga, tip.Id, opis));
		}

		public void Obrisi()
		{
			if (!Obrisan)
			{
				ProveriZakljucan();
				Obavesti(new NalogObrisan(Id));
			}
		}

		private void ProveriZakljucan()
		{
			if (Zakljucan)
				throw new KnjizenjeException("Nalog je zaključan");
		}

		private void ProveriStavkaPripadaNalogu(FinStavka stavka)
		{
			if (!stavke.Contains(stavka))
				throw new KnjizenjeException("Stavka ne pripada nalogu");
		}

		internal void Primeni(NalogOtvoren no)
		{
			Id = no.IdNaloga;
		}

		internal void Primeni(StavkaProknjizena sp)
		{
			var stavka = new FinStavka(sp.IdStavke, sp.IdKonto, sp.Duguje, sp.Potrazuje,
				sp.Opis, sp.DatumKnjizenja);
			stavke.Add(stavka);
		}

		internal void Primeni(StavkaUklonjena su)
		{
			var stavka = stavke.Where(x => x.Id == su.IdStavke).Single();
			stavke.Remove(stavka);
		}

		internal void Primeni(NalogZakljucan nz)
		{
			Zakljucan = true;
		}

		internal void Primeni(NalogOtkljucan no)
		{
			Zakljucan = false;
		}

		internal void Primeni(NalogObrisan no)
		{
			Obrisan = true;
		}
	}
}
