﻿using Knjizenje.Domain.Entities.KontoAggregate;
using Knjizenje.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.Entities.FinNalogAggregate
{
	public class FinStavka : Entity<Guid>
	{
		public long IdKonto { get; private set; }
		public DateTime DatumKnjizenja { get; private set; }
		public IznosStavke Iznos { get; private set; }
		public string Opis { get; private set; }

		internal FinStavka(Guid id, long idKonto, decimal duguje, decimal potrazuje,
			string opis, DateTime datumKnjizenja) : this(id, idKonto, duguje, potrazuje, opis)
		{
			this.DatumKnjizenja = datumKnjizenja;
		}

		internal FinStavka(Guid id, long idKonto, decimal duguje, decimal potrazuje,
			string opis)
		{
			this.Id = id;
			this.IdKonto = idKonto;
			this.Iznos = new IznosStavke(duguje, potrazuje);
			this.Opis = opis;
		}

		public static FinStavka Nova(long idKonto, decimal duguje, decimal potrazuje,
			string opis)
		{
			return new FinStavka(Guid.NewGuid(), idKonto, duguje, potrazuje, opis);
		}
	}
}
