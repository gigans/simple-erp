﻿using Knjizenje.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.Entities.KontoAggregate
{
    public class Konto : Entity<long>
	{
		public string Sifra { get; private set; }
		public string Naziv { get; private set; }

		private Konto() { }

		public Konto(string sifra, string naziv)
		{
			this.Sifra = sifra;
			this.Naziv = naziv;
		}
	}
}
