﻿using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Domain.Entities.KontoAggregate
{
	public interface IKontoRepository : IRepository<Konto>
	{
		Task<Konto> GetBySifraAsync(string sifra);
	}
}
