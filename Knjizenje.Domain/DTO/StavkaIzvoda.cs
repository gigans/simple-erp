﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.DTO
{
    public class StavkaIzvoda
    {
		public int SifraPlacanja { get; set; }
		public decimal Duguje { get; set; }
		public decimal Potrazuje { get; set; }

		public StavkaIzvoda(int sifraPlacanja, decimal duguje, decimal potrazuje)
		{
			this.SifraPlacanja = sifraPlacanja;
			this.Duguje = duguje;
			this.Potrazuje = potrazuje;
		}
	}
}
