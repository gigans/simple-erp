﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.SeedWork
{
	public class EventBase
    {
		public Guid EventId { get; set; }
		public long EventNumber { get; set; } = -1;
    }
}
