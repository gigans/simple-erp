﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.Domain.SeedWork
{
	public interface IAggregateRepository<TAggregate, TKey>
		where TKey : IAggregateId
	{
		Task<TAggregate> GetAsync(TKey id);
		Task SaveAsync(AggregateRoot<TAggregate, TKey> aggregate, Guid commandId);
	}
}
