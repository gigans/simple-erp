﻿using Knjizenje.Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;
using System.Runtime.Serialization;

namespace Knjizenje.Domain.SeedWork
{
	public class AggregateRoot<TEntity, TKey> : Entity<TKey>, IAggregateRoot
	{
		private static ConstructorInfo constructor;
		private static List<MethodInfo> applyMethods;

		private List<EventBase> uncommittedEvents = new List<EventBase>();
		public IReadOnlyCollection<EventBase> UncommittedEvents => uncommittedEvents.AsReadOnly();
		public long Version { get; private set; } = -1;

		protected void Obavesti<TEvent>(TEvent @event)
			where TEvent : EventBase
		{
			@event.EventId = Guid.NewGuid();
			ApplyEvent<TEvent>(@event);
			uncommittedEvents.Add(@event);
		}

		public void ApplyEvent<TEvent>(TEvent @event)
			where TEvent : EventBase
		{
			InitApplyMethods();
			var apply = FindApplyMethod(@event.GetType());
			if (apply != null)
				apply.Invoke(this, new[] { @event });
			if (@event.EventNumber >= 0)
				Version = @event.EventNumber;
		}

		private void InitApplyMethods()
		{
			if (applyMethods == null)
			{
				applyMethods = this.GetType().GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
					.Where(x => x.Name == "Primeni" && 
						x.GetParameters().Where(p => p.ParameterType.IsSubclassOf(typeof(EventBase))).Count() == 1)
					.ToList();
			}
		}

		private MethodInfo FindApplyMethod(Type inputType)
		{
			return applyMethods.Where(m => m.GetParameters().Single().ParameterType == inputType).SingleOrDefault();
		}

		public void ClearUncommittedEvents()
		{
			uncommittedEvents.Clear();
		}

		public static AggregateRoot<TEntity,TKey> CreateEmpty()
		{
			Type entityType = typeof(TEntity);
			if (constructor == null)
				constructor = entityType
					.GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public,
						null, new Type[0], new ParameterModifier[0]);

			if (constructor == null)
				throw new InvalidOperationException($"Parameterless constructor for type {entityType.Name} not found");

			return (AggregateRoot<TEntity, TKey>)constructor.Invoke(new object[0]);
		}
	}
}
