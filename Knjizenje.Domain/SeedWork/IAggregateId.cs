﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Knjizenje.Domain.SeedWork
{
	public interface IAggregateId
	{
		string IdAsString();
		Guid Id { get; }
	}
}
