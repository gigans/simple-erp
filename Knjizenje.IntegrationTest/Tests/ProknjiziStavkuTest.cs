using Knjizenje.API.Application.Commands.ProknjiziStavku;
using Knjizenje.API.Application.EventBus.ProknjiziStavku;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class ProknjiziStavkuTest
	{
		[Fact]
		public async Task ProknjiziStavku_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			FinNalogId idNaloga = new FinNalogId(Guid.NewGuid().ToString());
			Guid idStavke = Guid.NewGuid();
			await SeedEvents.Seed(idNaloga.IdAsString(), new EventBase[]
			{
				new NalogOtvoren(idNaloga, new DateTime(2018, 10, 14), TipNaloga.UlazneFakture.Id, "opis naloga"),
			});
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new ProknjiziStavkuMessage()
			{
				CommandId = commandId,
				IdNaloga = idNaloga.Id,
				IdKonto = 2,
				Duguje = 0,
				Potrazuje = 88.53m,
				Opis = "nova stavka"
			});

			EventAssert.OfType<StavkaProknjizena>(eventQueue, (e) =>
			{
				Assert.Equal(1, e.EventNumber);
				Assert.Equal(idNaloga, e.IdNaloga);
				Assert.Equal(2, e.IdKonto);
				Assert.Equal(0, e.Duguje);
				Assert.Equal(88.53m, e.Potrazuje);
				Assert.Equal("nova stavka", e.Opis);
			});
		}
	}
}
