using Knjizenje.API.Application.Commands.ZakljucajNalog;
using Knjizenje.API.Application.EventBus.ZakljucajNalog;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class ZakljucajNalogTest
	{
		[Fact]
		public async Task ZakljucajNalog_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			FinNalogId idNaloga = new FinNalogId(Guid.NewGuid().ToString());

			await SeedEvents.Seed(idNaloga.IdAsString(), new[]
			{
				new NalogOtvoren(idNaloga, new DateTime(2018, 10, 14), TipNaloga.UlazneFakture.Id, "opis naloga")
			});
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new ZakljucajNalogMessage()
			{
				CommandId = commandId,
				IdNaloga = idNaloga.Id
			});

			EventAssert.OfType<NalogZakljucan>(eventQueue, (e) =>
			{
				Assert.Equal(1, e.EventNumber);
				Assert.Equal(idNaloga, e.IdNaloga);
			});
		}
	}
}
