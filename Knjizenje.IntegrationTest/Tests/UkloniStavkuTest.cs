using Knjizenje.API.Application.Commands.UkloniStavku;
using Knjizenje.API.Application.EventBus.UkloniStavku;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class UkloniStavkuTest
	{
		[Fact]
		public async Task UkloniStavku_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			FinNalogId idNaloga = new FinNalogId(Guid.NewGuid().ToString());
			Guid idStavke = Guid.NewGuid();
			await SeedEvents.Seed(idNaloga.IdAsString(), new EventBase[]
			{
				new NalogOtvoren(idNaloga, new DateTime(2018, 10, 14), TipNaloga.UlazneFakture.Id, "opis naloga"),
				new StavkaProknjizena(idNaloga, Guid.NewGuid(), new DateTime(2018,10,14), 1, 45.6m, 0, null),
				new StavkaProknjizena(idNaloga, idStavke, new DateTime(2018,10,14), 1, 0, 56.5m, null)
			});
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new UkloniStavkuMessage()
			{
				CommandId = commandId,
				IdNaloga = idNaloga.Id,
				IdStavke = idStavke
			});

			EventAssert.OfType<StavkaUklonjena>(eventQueue, (e) =>
			{
				Assert.Equal(3, e.EventNumber);
				Assert.Equal(idNaloga, e.IdNaloga);
				Assert.Equal(idStavke, e.IdStavke);
			});
		}
	}
}
