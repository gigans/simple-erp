using Integration.Contracts.Messages;
using Knjizenje.API.Application.Commands.OtvoriNalog;
using Knjizenje.API.Application.EventBus.OtvoriNalog;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using Moq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class OtvoriNalogTest
	{
		[Fact]
		public async Task OtvoriNalog_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			var stavka = new Mock<IStavka>();
			stavka.SetupGet(x => x.IdKonto).Returns(1);
			stavka.SetupGet(x => x.Duguje).Returns(250);
			stavka.SetupGet(x => x.Potrazuje).Returns(0);
			stavka.SetupGet(x => x.Opis).Returns("test opis stavke");
			await EventBusClient.Instance.Send(new OtvoriNalogMessage()
			{
				CommandId = commandId,
				DatumNaloga = new DateTime(2018, 12, 12),
				IdTip = TipNaloga.UlazneFakture.Id,
				Opis = "test opis naloga",
				Stavke = new List<IStavka>
				{
					stavka.Object
				}
			});

			EventAssert.OfType<NalogOtvoren>(eventQueue, (e) =>
			{
				Assert.Equal(0, e.EventNumber);
				Assert.Equal(new DateTime(2018, 12, 12), e.DatumNaloga);
				Assert.Equal(TipNaloga.UlazneFakture.Id, e.IdTip);
			});
			EventAssert.OfType<StavkaProknjizena>(eventQueue,
				(e) =>
				{
					Assert.Equal(1, e.EventNumber);
					Assert.Equal(1, e.IdKonto);
					Assert.Equal(250, e.Duguje);
					Assert.Equal(0, e.Potrazuje);
					Assert.Equal("test opis stavke", e.Opis);
				});
		}
	}
}
