using Knjizenje.API.Application.Commands.IzmeniZaglavljeNaloga;
using Knjizenje.API.Application.EventBus.IzmeniZaglavljeNaloga;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class IzmeniZaglavljeNalogaTest
	{
		[Fact]
		public async Task IzmeniZaglavljeNaloga_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			FinNalogId idNaloga = new FinNalogId(Guid.NewGuid().ToString());

			await SeedEvents.Seed(idNaloga.IdAsString(), new[]
			{
				new NalogOtvoren(idNaloga, new DateTime(2018, 10, 14), TipNaloga.UlazneFakture.Id, "opis naloga")
			});
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new IzmeniZaglavljeNalogaMessage()
			{
				CommandId = commandId,
				IdNaloga = idNaloga.Id,
				DatumNaloga = new DateTime(2018, 11, 15),
				IdTip = TipNaloga.Izvodi.Id,
				Opis = "novi opis naloga"
			});

			EventAssert.OfType<IzmenjenoZaglavljeNaloga>(eventQueue, (e) =>
			{
				Assert.Equal(1, e.EventNumber);
				Assert.Equal(idNaloga, e.IdNaloga);
				Assert.Equal(new DateTime(2018, 11, 15), e.DatumNaloga);
				Assert.Equal(TipNaloga.Izvodi.Id, e.IdTip);
				Assert.Equal("novi opis naloga", e.Opis);
			});
		}
	}
}
