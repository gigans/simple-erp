using Knjizenje.API.Application.Commands.StornirajStavku;
using Knjizenje.API.Application.EventBus.StornirajStavku;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class StornirajStavkuTest
	{
		[Fact]
		public async Task StornirajStavku_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			FinNalogId idNaloga = new FinNalogId(Guid.NewGuid().ToString());
			Guid idStavke = Guid.NewGuid();
			await SeedEvents.Seed(idNaloga.IdAsString(), new EventBase[]
			{
				new NalogOtvoren(idNaloga, new DateTime(2018, 10, 14), TipNaloga.UlazneFakture.Id, "opis naloga"),
				new StavkaProknjizena(idNaloga, idStavke, new DateTime(2018,10,14), 1, 45.6m, 0, null)
			});
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new StornirajStavkuMessage()
			{
				CommandId = commandId,
				IdNaloga = idNaloga.Id,
				IdStavke = idStavke
			});

			EventAssert.OfType<StavkaProknjizena>(eventQueue, (e) =>
			{
				Assert.Equal(2, e.EventNumber);
				Assert.Equal(idNaloga, e.IdNaloga);
				Assert.Equal(1, e.IdKonto);
				Assert.Equal(-45.6m, e.Duguje);
				Assert.Equal(0, e.Potrazuje);
			});
		}
	}
}
