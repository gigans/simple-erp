using Knjizenje.API.Application.Commands.ProknjiziIzvod;
using Knjizenje.API.Application.EventBus.ProknjiziIzvod;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class ProknjiziIzvodTest
	{
		[Fact]
		public async Task ProknjiziIzvod_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new ProknjiziIzvodMessage()
			{
				CommandId = commandId,
				Datum = new DateTime(2018, 12, 12),
				Stavke = new[]
				{
					new StavkaIzvodaDTO(221, 250, 0)
				}
			});

			EventAssert.OfType<NalogOtvoren>(eventQueue, (e) =>
			{
				Assert.Equal(0, e.EventNumber);
				Assert.Equal(new DateTime(2018, 12, 12), e.DatumNaloga);
				Assert.Equal(TipNaloga.Izvodi.Id, e.IdTip);
			});
			EventAssert.OfType<StavkaProknjizena>(eventQueue,
				(e) =>
				{
					Assert.InRange(e.EventNumber, 1, 2);
					Assert.Equal(250, e.Potrazuje);
					Assert.Equal(0, e.Duguje);
				},
				(e) =>
				{
					Assert.InRange(e.EventNumber, 1, 2);
					Assert.Equal(250, e.Duguje);
					Assert.Equal(0, e.Potrazuje);
				});
		}
	}
}
