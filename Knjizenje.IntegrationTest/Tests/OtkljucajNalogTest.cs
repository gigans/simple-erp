using Knjizenje.API.Application.Commands.OtkljucajNalog;
using Knjizenje.API.Application.EventBus.OtkljucajNalog;
using Knjizenje.Domain.Entities.FinNalogAggregate;
using Knjizenje.Domain.Events;
using Knjizenje.Domain.SeedWork;
using Knjizenje.IntegrationTest.Seedwork;
using MassTransit;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;

namespace Knjizenje.IntegrationTest.Tests
{
	public class OtkljucajNalogTest
	{
		[Fact]
		public async Task OtkljucajNalog_Uspesno()
		{
			await TestHelper.Instance.WaitForApiAsync();
			FinNalogId idNaloga = new FinNalogId(Guid.NewGuid().ToString());

			await SeedEvents.Seed(idNaloga.IdAsString(), new[]
			{
				new NalogOtvoren(idNaloga, new DateTime(2018, 10, 14), TipNaloga.UlazneFakture.Id, "opis naloga")
			});
			Guid commandId = Guid.NewGuid();
			ConcurrentQueue<EventBase> eventQueue = new ConcurrentQueue<EventBase>();
			await EventStoreClient.Instance.SubscribeToCommandChanges(commandId, eventQueue);

			await EventBusClient.Instance.Send(new OtkljucajNalogMessage()
			{
				CommandId = commandId,
				IdNaloga = idNaloga.Id
			});

			EventAssert.OfType<NalogOtkljucan>(eventQueue, (e) =>
			{
				Assert.Equal(1, e.EventNumber);
				Assert.Equal(idNaloga, e.IdNaloga);
			});
		}
	}
}
