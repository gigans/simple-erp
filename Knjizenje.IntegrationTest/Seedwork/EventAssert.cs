﻿using Knjizenje.Domain.SeedWork;
using Polly;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Sdk;

namespace Knjizenje.IntegrationTest.Seedwork
{
    public static class EventAssert
    {
		private static Policy assertPolicy;

		static EventAssert()
		{
			assertPolicy = CreateAssertPolicy();
		}

		private static Policy CreateAssertPolicy()
		{
			return Policy
				.Handle<EqualException>()
				.Or<TrueException>()
				.WaitAndRetry(20, retryAttempt =>
					TimeSpan.FromSeconds(1));
		}

		public static void All(ConcurrentQueue<EventBase> eventQueue, Action<EventBase> assert,
			params Action<EventBase>[] eventInspectors)
		{
			int expectedEventsCount = eventInspectors.Count();
			assertPolicy.Execute(() => Assert.Equal(expectedEventsCount, eventQueue.Count));
			Assert.Collection(eventQueue, eventInspectors);
		}

		public static void OfType<TEvent>(ConcurrentQueue<EventBase> eventQueue, 
			params Action<TEvent>[] eventInspectors)
			where TEvent : EventBase
		{
			List<TEvent> eventsOfType = new List<TEvent>();
			int expectedEventsCount = eventInspectors.Count();
			assertPolicy.Execute(() =>
			{
				EventBase[] events = new EventBase[eventQueue.Count];
				eventQueue.CopyTo(events, 0);
				eventsOfType = events.OfType<TEvent>().ToList();
				Assert.True(expectedEventsCount == eventsOfType.Count, 
					$"Number of expected events of type {typeof(TEvent).Name}: {expectedEventsCount}, actual: {eventsOfType.Count}");
			});
			Assert.Collection(eventsOfType, eventInspectors);
		}
    }
}
