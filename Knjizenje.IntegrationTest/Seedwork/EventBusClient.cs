﻿using MassTransit;
using MassTransit.RabbitMqTransport;
using MassTransit.Util;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.IntegrationTest.Seedwork
{
	public class EventBusClient
	{
		private static Lazy<EventBusClient> instance = new Lazy<EventBusClient>(() => new EventBusClient());
		public static EventBusClient Instance => instance.Value;
		private static SemaphoreSlim initializeSemaphore = new SemaphoreSlim(1, 1);

		private IBusControl busControl;
		private ISendEndpoint sendEndpoint = null;

		private EventBusClient()
		{
			busControl = CreateAndStartBus();
		}

		private static IBusControl CreateAndStartBus()
		{
			return Policy
				.Handle<RabbitMqConnectionException>()
				.WaitAndRetry(5, retryAttempt =>
					TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)))
					.Execute<IBusControl>(() =>
					{
						var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
						{
							var hostEndpoint = cfg.Host(new Uri($"rabbitmq://rabbitmq"), h =>
							{
								h.Username("guest");
								h.Password("guest");
							});
						});
						var busHandle = TaskUtil.Await(() => busControl.StartAsync());
						return busControl;
					});
		}

		public async Task Send(object message)
		{
			await InitializeSendEndpoint();
			await sendEndpoint.Send(message);
		}

		private async Task InitializeSendEndpoint()
		{
			if (sendEndpoint == null)
			{
				await initializeSemaphore.WaitAsync();
				try
				{
					if (sendEndpoint == null)
						sendEndpoint = await busControl.GetSendEndpoint(new Uri("rabbitmq://rabbitmq/nalozi_queue"));
				}
				finally
				{
					initializeSemaphore.Release();
				}
			}
		}
	}
}
