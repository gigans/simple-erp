﻿using EventStore.ClientAPI;
using Knjizenje.Domain.SeedWork;
using Knjizenje.Infrastructure.EventSourcing;
using Knjizenje.Infrastructure.EventSourcing.Serialization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizenje.IntegrationTest.Seedwork
{
    public class SeedEvents
    {
		public static async Task Seed(string stream, IEnumerable<EventBase> events)
		{
			var eventSerializer = new EventSerializerContractResolver();
			var eventJsonSettings = new JsonSerializerSettings()
			{
				ContractResolver = eventSerializer
			};
			var metadataJson = JsonConvert.SerializeObject(new EventMetadata(Guid.NewGuid()));
			var eventMetadata = Encoding.UTF8.GetBytes(metadataJson);
			var eventData = events.Select(x =>
			{
				x.EventId = Guid.NewGuid();
				string json = JsonConvert.SerializeObject(x, eventJsonSettings);
				var serializedEvent = Encoding.UTF8.GetBytes(json);

				return new EventData(x.EventId, x.GetType().Name, true, serializedEvent, eventMetadata);
			});
			await EventStoreClient.Instance.Connection.AppendToStreamAsync(stream, ExpectedVersion.NoStream, eventData);
		}
	}
}
