﻿using Polly;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Knjizenje.IntegrationTest.Seedwork
{
	public class TestHelper
	{
		private static TestHelper instance;

		public static TestHelper Instance
		{
			get
			{
				if (instance == null)
					instance = new TestHelper();
				return instance;
			}
		}

		private HttpClient apiClient;
		private Policy checkApiPolicy;

		private TestHelper()
		{
			apiClient = new HttpClient();
			checkApiPolicy = CreateCheckApiPolicy();
		}

		private Policy CreateCheckApiPolicy()
		{
			return Policy
				.Handle<HttpRequestException>()
				.WaitAndRetryAsync(20,
					retryAttempt => TimeSpan.FromSeconds(2));
		}

		public async Task WaitForApiAsync()
		{
			await checkApiPolicy.ExecuteAsync(async () =>
			{
				var response = await apiClient.SendAsync(new HttpRequestMessage(HttpMethod.Head,
					"http://knjizenje.api/api/status/check"));
				response.EnsureSuccessStatusCode();
			});
		}
    }
}
