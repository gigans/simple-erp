﻿using EventStore.ClientAPI;
using EventStore.ClientAPI.SystemData;
using Knjizenje.Domain.SeedWork;
using Knjizenje.Infrastructure.EventSourcing;
using Knjizenje.Infrastructure.EventSourcing.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Knjizenje.IntegrationTest.Seedwork
{
	public class EventStoreClient
	{
		private static Lazy<EventStoreClient> instance = new Lazy<EventStoreClient>(() => new EventStoreClient());
		public static EventStoreClient Instance => instance.Value;
		private static SemaphoreSlim subscribeSemaphore = new SemaphoreSlim(1, 1);
		private bool isSubscribed = false;

		public IEventStoreConnection Connection { get; }
		private IList<Type> eventTypes;
		private Dictionary<Guid, ConcurrentQueue<EventBase>> queues;


		private EventStoreClient()
		{
			eventTypes = typeof(EventBase).Assembly.GetTypes().Where(x => x.IsSubclassOf(typeof(EventBase))).ToList();
			queues = new Dictionary<Guid, ConcurrentQueue<EventBase>>();
			Connection = CreateClient();
		}

		private static IEventStoreConnection CreateClient()
		{
			var client = EventStoreConnection.Create(new Uri($"tcp://eventstore:1113"));
			client.ConnectAsync().Wait();
			return client;
		}

		public async Task SubscribeToCommandChanges(Guid commandId, ConcurrentQueue<EventBase> eventQueue)
		{
			await subscribeSemaphore.WaitAsync();
			try
			{
				if (!isSubscribed)
					await SubscribeToAll();
				queues.Add(commandId, eventQueue);
			}
			finally
			{
				subscribeSemaphore.Release();
			}
		}

		private async Task SubscribeToAll()
		{
			await Connection.SubscribeToAllAsync(false, (s, e) =>
			{
				string metadataJson = Encoding.UTF8.GetString(e.Event.Metadata);
				EventMetadata metadata = null;
				try
				{
					metadata = JsonConvert.DeserializeObject<EventMetadata>(metadataJson);
				}
				catch { }
				if (metadata == null || !queues.ContainsKey(metadata.CommandId))
					return;
				var queue = queues[metadata.CommandId];
				Type eventType = null;
				try
				{
					string dataJson = Encoding.UTF8.GetString(e.Event.Data);
					Console.WriteLine($"{e.Event.EventType} : {metadata.CommandId}");
					Console.WriteLine(JToken.Parse(dataJson).ToString(Formatting.Indented));
					eventType = eventTypes.Where(x => x.Name == e.Event.EventType).Single();
					var jsonSettings = new JsonSerializerSettings()
					{
						ContractResolver = new EventSerializerContractResolver()
					};
					//jsonSettings.Converters.Add(new AggregateIdConverter());
					var @event = (EventBase)JsonConvert.DeserializeObject(dataJson, eventType, jsonSettings);
					@event.EventNumber = e.Event.EventNumber;
					queue.Enqueue(@event);
				}
				catch (Exception exc)
				{
					Console.WriteLine($"Error deserializing event of type {e.Event.EventType}");
					Console.WriteLine(exc.Message);
					Console.WriteLine(exc.StackTrace);
				}
			}, null, new UserCredentials("admin", "changeit"));
			isSubscribed = true;
		}
	}
}
